import networkx as nx
from abc import ABC, abstractmethod


# Classe base per l'implementazione dei vari algoritmi
class BicriterionBase(ABC):
    def __init__(self, wave_net: nx.DiGraph):
        self.wave_net = wave_net
        self.source = 's_1491'
        self.target = 't_1611'

        self.tempo_esecuzione = 0
        super().__init__()

    @abstractmethod
    def search_paths(self):
        pass
