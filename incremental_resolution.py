from typing import ClassVar
from graphs.netfactory_onoc import ONOCNetFactory
import numpy as np


class IncrementalSolver:
    def __init__(self, solver_class: ClassVar, net_baker: ClassVar, radii: int, resonances: int):
        from graphs.risonanze import NetConfig
        c = NetConfig()
        self.net_factory: ONOCNetFactory = net_baker(c)
        self.solver_class = solver_class
        self.radii = radii
        self.resonances = resonances
        self.incremental_models = []

    def find_optimal(self):
        tau = 0.1
        tau_max = self.net_factory.upper_tau(self.net_factory.delta_gap(self.radii, self.resonances))
        # Se il numero di archi resta invariato posso evitare di cercare il cammino di nuovo
        prev_edges = -1

        while tau <= tau_max:
        #for tau in np.linspace(0.1, tau_max, 3):
            model_result = self._solve_subgraph_problem(prev_edges, tau)
            if model_result is not None:
                prev_edges = model_result[3]
                IncrementalSolver.log_to_file([model_result],
                            self.solver_class.log_filename())
                self.incremental_models.append(model_result)
                if model_result[5] >= 0:
                    if model_result[5] > 2*tau:
                        tau = model_result[5]/2
                        model_result = self._solve_subgraph_problem(-1, tau)
                        IncrementalSolver.log_to_file([model_result],
                                                      self.solver_class.log_filename())
                        self.incremental_models.append(model_result)

                    break

            tau += 0.1
        """
        if tau < tau_max:
            pass
            self.incremental_models.append(self._solve_subgraph_problem(-1))

        """
    def _solve_subgraph_problem(self, prev_edges, tau=None):
        network = self.net_factory.make_wave_net(self.radii, self.resonances, tau=tau)
        if len(network.G.edges) == prev_edges:
            return None
        model = None
        nodes_path = []
        if self.solver_class == WaveSelection3DMatrixBidirectionalSearchSingle:
            rev_network = self.net_factory.make_wave_net(self.radii, self.resonances, tau=tau, reverse=True)
            model = self.solver_class(network, rev_network)
        else:
            model = self.solver_class(network)
        try:
            print(f"Solving radii={self.radii}, resonance={self.resonances}, tau={tau}")
            model.solve_model()
            nodes_path = model.get_path()
        except ValueError:
            print("Il modello non ha soluzioni.")

        return (f"{model.net.radii}x{model.net.resonances}", str(model.net.delta)[:4], str(model.net.tau)[:5],
                len(model.net.G.edges), str(model.get_runtime())[:5], model.obj_value(), str(nodes_path))

    @staticmethod
    def init_log_file(filename='results'):
        f = open(f"{filename}.txt", "w+")
        f.write("")
        f.close()

    @staticmethod
    def log_to_file(solved_models, filename='results'):
        f = open(f"{filename}.txt", "a")
        f.write(f"{'Dimensioni':^10} {'Delta':^10} "
                f"{'Tau':^10} {'NumArchi':^10} {'Time':^10} {'CostoSoluzione':^20}\n")
        for result in solved_models:
            f.write(f"{result[0]:^10} "
                    f"{result[1]:>10} "
                    f"{result[2]:>10} "
                    f"{result[3]:>10} "
                    f"{result[4]:>10} "
                    f"{str(result[5])[:5]:>10}\n")
        f.write(f"{'Dimensioni':^10} "
                f"{'Tau':^10} {'Time':^10} {'CostoSoluzione':^20} Path\n")
        for result in solved_models:
            f.write(f"{result[0]:^10} "
                    f"{result[2]:>10} "
                    f"{result[4]:>10} "
                    f"{str(result[5])[:4]:>10} {result[6]}\n")

        f.write("\n")
        f.close()


if __name__ == '__main__':
    from ilpmodel.gurobi_milp import GrbIlp
    from ilpmodel.ortools_milp import WSPIlp
    from dynamicprogramming.shortest_path_k_edges3d_sorting import ShortestPathKEdges3DSorting
    from dynamicprogramming.yen_netx_wave_selection import YenNetXWaveSelection
    from dynamicprogramming.wave_selection_3d_matrix_routing_fault_parallel_look_ahead_dominance import \
        WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance
    from dynamicprogramming.wave_selection_3d_matrix_bidirectional_search_single \
        import WaveSelection3DMatrixBidirectionalSearchSingle

    plls = [8] * 1
    plls.sort()
    cases = list(zip([4] * 1, plls))
    cases.sort()
    incremental_models = []

    #IncrementalSolver.init_log_file(WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance.log_filename())
    for rd, pll in cases:
        inc_model = IncrementalSolver(WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance, ONOCNetFactory, rd, pll)
        inc_model.find_optimal()
        #IncrementalSolver.log_to_file(inc_model.incremental_models,
        #                              WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance.log_filename())
        #incremental_models += inc_model.incremental_models
