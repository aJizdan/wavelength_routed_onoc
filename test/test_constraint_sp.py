import unittest

from label import Label
from constraint_sp import ConstraintSP


class TestRaySelection(unittest.TestCase):
    def setUp(self):
        from graphs.wavenet import WaveNet
        from graphs.risonanze import DATA, SOURCE, TARGET

        wavelength_net = WaveNet(DATA, SOURCE, TARGET)
        wavelength_net.select_rays(limit_radii=2, num_nodes_per_radii=120)
        wavelength_net.create_graph(tau=37, )
        # wavelength_net.add_cost_attributes()

        self.csp = ConstraintSP(wavelength_net)

        self.labels_list = []
        self.node = 's_1491'
        self.label1 = Label(1, 2, self.node, None, 0, None)

        self.label2 = Label(2, 4, self.node, None, 0, None)

    def test_label_list_push(self):
        self.csp.heap_push(self.labels_list, self.label1)
        self.assertIn(self.label1, self.labels_list)
        self.assertEqual([self.label1], self.labels_list)

        self.csp.heap_push(self.labels_list, self.label2)
        self.assertIn(self.label2, self.labels_list)
        self.assertEqual([self.label1, self.label2], self.labels_list)
        self.assertEqual([self.label1, self.label2], self.csp.wave_net.node[self.node]['labels'])

    def test_label_list_pop(self):
        self.csp.heap_push(self.labels_list, self.label1)
        self.csp.heap_push(self.labels_list, self.label2)

        current_label = self.csp.heap_pop(self.labels_list)

        self.assertNotIn(current_label, self.labels_list)
        self.assertNotIn(current_label, self.csp.wave_net.node[self.node]['labels'])

    def test_delete_from_labels_list(self):
        self.csp.heap_push(self.labels_list, self.label1)
        self.csp.heap_push(self.labels_list, self.label2)

        self.csp.remove_label(self.labels_list, self.label1)

        self.assertNotIn(self.label1, self.labels_list)
        self.assertNotIn(self.label1, self.csp.wave_net.node[self.node]['labels'])

    def test_create_path_from_not_target_label(self):
        permanete_labels = [self.label1]
        with self.assertRaises(ValueError):
            self.csp.create_path(permanete_labels, self.label1)

    def test_create_path_from_target_label(self):
        pass

    def test_look_ahead_corectness(self):
        pass
