import unittest

from constraint_sp import ConstraintSP
from dijakstra_pq import DijakstraPQ
from graphs.path import Path
from test.assert_not_routing_fault import RoutingAssertions

class TestBicriterionAlgorithm(unittest.TestCase, RoutingAssertions):
    def setUp(self):
        from graphs.wavenet import WaveNet
        from graphs.risonanze import SOURCE, TARGET

        DATA = {
            "0": [1496, 1521], "1": [1521, 1600]
        }

        self.wavelength_net = WaveNet(DATA, SOURCE, TARGET, nradii=2, nresonance=1)
        self.wavelength_net.select_rays()
        self.wavelength_net.create_graph(tau=100, )

        self.wavelength_net.add_routing_fault_radii()
        self.wavelength_net.add_look_ahead()
        self.wavelength_net.add_cost_attributes()
        self.csp = ConstraintSP(self.wavelength_net)

    def test_routing_fault_prevention(self):
        bc_pq = DijakstraPQ(self.wavelength_net.G)
        # Controllo che la rete sia connessa
        sht_path: Path = bc_pq.search_paths()

        path = self.csp.search_paths()

        self.assertEqual((self.wavelength_net.nradii * self.wavelength_net.nresonance), len(path.nodes)-2)

        for nd in path.nodes:
            with self.subTest(node=nd):
                self.assertNotRoutingFault(path, nd)
