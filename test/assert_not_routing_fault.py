
class RoutingAssertions:

    def assertNotRoutingFault(self, path, nd):
            if len(self.wavelength_net.G.node[nd]['conflict_radii']) > 0:
                pt_rays = set(path.find_rays().keys())
                if len(list(pt_rays & set(self.wavelength_net.G.node[nd]['conflict_radii']))) != 0:
                    raise AssertionError(f"Il cammino {path.nodes} produce routing fault per il nodo {nd}!")

