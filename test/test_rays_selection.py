import unittest

from graphs.wavenet import WaveNet
from graphs.risonanze import DATA, SOURCE, TARGET


class TestRaySelection(unittest.TestCase):
    def setUp(self):
        self.wave_net = WaveNet(DATA, SOURCE, TARGET)

    def test_della_selezione_dei_raggi_dal_file(self):
        """
        Test del metodo per la selezione dei raggi e delle risonanze
        """
        self.wave_net.select_rays()

        self.assertEqual(len(self.wave_net.rays), len(DATA.keys()))

        for max_radii in range(0,300):
            with self.subTest(max_radii=max_radii):
                self.wave_net.select_rays(limit_radii=max_radii)
                if max_radii >= len(DATA.keys()):
                    self.assertEqual(len(self.wave_net.rays), len(DATA))
                else:
                    self.assertEqual(len(self.wave_net.rays), max_radii)

    def test_selezione_raggi_e_limitazione_risonanze(self):
        """
        Test per la limitazione sia dei raggi che delle risonanze
        """

        delta_risonanze = (TARGET['freq'] - SOURCE['freq']) + 10
        for max_resonaces in range(1,delta_risonanze):
            with self.subTest(max_resonaces=max_resonaces):
                for key in list(DATA.keys()):
                    selected_res = self.wave_net.equi_list_slicer(DATA[key], max_resonaces)
                    if len(DATA[key]) <= max_resonaces:
                        self.assertEqual(len(selected_res), len(DATA[key]))
                    else:
                        self.assertEqual(len(selected_res), max_resonaces)

        self.wave_net.select_rays(limit_radii=2, num_nodes_per_radii=3)

        self.assertEqual(len(self.wave_net.rays), 2)
        for ray in self.wave_net.rays:
            with self.subTest(ray=ray):
                self.assertEqual(len(ray), 3)

    def test_creazione_del_grafo(self):
        """
        Test della conformità del grafo ai dati inseriti
        """

        self.wave_net.select_rays()

        self.wave_net.create_graph()

        # il grafo deve avere il nodo di source e target
        self.assertTrue(self.wave_net.G.has_node(SOURCE['label']))
        self.assertTrue(self.wave_net.G.has_node(TARGET['label']))

        self.assertEqual(len(self.wave_net.G.in_edges(SOURCE['label'])), 0)
        self.assertEqual(len(self.wave_net.G.out_edges(TARGET['label'])), 0)

        # tutti le risonanze devono diventare dei nodi
        sum_nodes = sum([len(DATA[ray]) for ray in DATA.keys()])
        self.assertEqual(len(self.wave_net.G.nodes), sum_nodes + 2)

    def test_look_ahead_aggiunta_risorse(self):
        """
        Test aggiunta conteggio dei picchi ancora disponibili per completare il cammino
        """
        self.wave_net.select_rays()
        self.wave_net.create_graph()

        self.wave_net.add_routing_fault_radii()
        self.wave_net.add_look_ahead()

        for node in self.wave_net.G.nodes:
            with self.subTest(node=node):
                self.assertEqual(len(list(self.wave_net.G.node[node]['resources'].keys())),
                                 len(list(self.wave_net.rays)))


if __name__ == '__main__':
    unittest.main()
