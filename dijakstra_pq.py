import numpy as np
import click
from utils import all_colors
from heapq import *
from bicriterion_base import BicriterionBase
from graphs.path import Path


# Algoritmo di Dijakstra, il gafo è diretto aciclico con il costo per ogni arco maggiore di zero.
# Le caratteristiche del grafo ci fornisce la condizione di ottimalità, poichè da un qualunque nodo
# estratto posso raggiungere solamente nodi che hanno un costo maggiore. Visito ogni nodo una sola volta.
class DijakstraPQ(BicriterionBase):
    def search_paths(self, **kwargs):
        self.wave_net.node[self.source]['cost'] = 0

        pq = [(0, self.source)]  # aggiungo nodo iniziale alla coda

        while pq:
            current_cost, current_node = heappop(pq)  # priority queue (estraggo l'etichetta con costo minore)

            # per ogni nodo estratto esamino i suoi nodi vicini
            vicini = self.wave_net.neighbors(current_node)
            for neighbor_key in vicini:
                neighbor_cost = self.wave_net.node[current_node]['cost'] + \
                                self.wave_net.get_edge_data(current_node, neighbor_key)[0]['weight']

                if neighbor_cost < self.wave_net.node[neighbor_key]['cost']:
                    if self.wave_net.node[neighbor_key]['cost'] == np.inf:
                        heappush(pq, (neighbor_cost, neighbor_key))

                    self.wave_net.node[neighbor_key]['cost'] = neighbor_cost
                    self.wave_net.node[neighbor_key]['prev'] = current_node

        return self.get_path()

    def get_path(self, ):
        path = Path([self.target], self.wave_net.node[self.target]['cost'])
        prev_node = self.wave_net.node[self.target]['prev']

        while prev_node:
            path.nodes.append(prev_node)
            if self.wave_net.node[prev_node]['prev'] is not None:
                prev_node = self.wave_net.node[prev_node]['prev']
            else:
                prev_node = None

        path.nodes.reverse()
        if path.nodes[0] != self.source or path.nodes[-1] != self.target:
            raise ValueError('Il grafo non è connesso, nessun cammino trovato!')
        return path


@click.command()
@click.option('--limit_radii', default=100, help='Numero di raggi da inserire nel grafo.')
@click.option('--nodes_in_radii', default=150, help='Numero di nodi per ogni raggio.')
@click.option('--tau', default=37, help='Peso massimo che un arco può raggiungere.')
def run_dijakstra(limit_radii, nodes_in_radii, tau):
    """Trovo il cammino con minor costo in base al peso degli archi del grafo."""
    from graphs.wavenet import WaveNet
    from graphs.risonanze import DATA, SOURCE, TARGET

    wavelength_net = WaveNet(DATA, SOURCE, TARGET)
    wavelength_net.select_rays(limit_radii=limit_radii, num_nodes_per_radii=nodes_in_radii)
    wavelength_net.create_graph(tau=tau, )
    wavelength_net.add_cost_attributes()

    bc_pq = DijakstraPQ(wavelength_net.G)
    try:
        sht_path: Path = bc_pq.search_paths()
    except Exception as ex:
        click.secho(str(ex), fg='red', bold=True)
        return

    print_path(sht_path)


def print_path(path: Path):
    click.secho(f"\tCosto del cammino: {path.cost}", fg='green', bold=True)
    for idx, node in enumerate(path.nodes):
        if node[0] == 's' or node[0] == 't':
            click.secho(f"\t[{node}]", fg='red', bold=True)
        else:
            click.secho(f"\t[{node}]", fg=all_colors[int(node.split('_')[0])])
        if idx+1 != len(path.nodes):
            click.secho('\t->', bold=True)


if __name__ == '__main__':
    run_dijakstra()
