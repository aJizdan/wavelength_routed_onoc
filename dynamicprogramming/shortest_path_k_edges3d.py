from dynamicprogramming.solver_base import SolverBase
from graphs.wavenet_model import WaveNetModel
import numpy as np


# Metodo  per la ricerca dei cammini minimi di lunghezza K,
# la complessità in caso di un grafo complettamente connesso può
# raggiungere O(V^3*K). Anche se in questo caso è sempre minore,
# poichè il grafo è aciclico orientato e con un ordinamento.
# Algoritmo adattato da:
# https://www.geeksforgeeks.org/shortest-path-exactly-k-edges-directed-weighted-graph/
class ShortestPathKEdges3D(SolverBase):
    def __init__(self, wavenet: WaveNetModel):
        super().__init__(wavenet)

    def solve_model(self) -> None:
        import time
        start = time.time()
        # Edges in path = radii*resonances+1
        k = self.net.radii * self.net.resonances + 1
        print(f"Cammino più corto ha costo: "
              f"{self._shortest_path(self.net.source, self.net.target, k)}")
        self.set_runtime(time.time() - start)

    def _shortest_path(self, source, target, k):
        nodelist = list(self.net.G.nodes())
        V = len(nodelist)
        sp_matrix = np.zeros((V, V, k+1))
        u = nodelist.index(source)
        v = nodelist.index(target)
        for e in range(k+1):
            for i in range(V):
                for j in range(V):

                    sp_matrix[i][j][e] = np.Inf
                    # Il primo strato deve essere a zero per nodi su se stessi
                    if e == 0 and i == j:
                        sp_matrix[i][j][e] = 0
                    if e == 1 and self.net.G.has_edge(nodelist[i], nodelist[j]):
                        sp_matrix[i][j][e] = self.net.G[nodelist[i]][nodelist[j]]['weight']

                    # Esploro gli archi adiacenti solamente quando il
                    # numero id archi è maggiore di 1
                    if e > 1:
                        for a in range(V):

                            # Cerco un arco da i ad a e un cammino di lunghezza (e-1) da a ad j, in modo da unirli
                            if (self.net.G.has_edge(nodelist[i], nodelist[a]) and i != a and
                                    j != a and sp_matrix[a][j][e - 1] != np.Inf):
                                sp_matrix[i][j][e] = min(sp_matrix[i][j][e],
                                                         self.net.G[nodelist[i]][nodelist[a]]['weight'] +
                                                         sp_matrix[a][j][e - 1])
        return sp_matrix[u][v][k]


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory

    c = cf.TestConfig()
    netfactory = ONOCNetFactory(c)

    net = netfactory.make_wave_net(2, 2)

    model = ShortestPathKEdges3D(net)

    model.solve_model()
    pass
