from dynamicprogramming.solver_base import SolverBase
from dynamicprogramming.dag_topo_sorting import DAGTopoSorting
from graphs.wavenet_model import WaveNetModel
import numpy as np


# Metodo  per la ricerca dei cammini minimi di lunghezza K,
# la complessità in caso di un grafo complettamente connesso può
# raggiungere O(V^3*K). Possiamo sfruttare l'ordinamento topologico
# dei nodi per ridurre lo spazio di ricerca dei cammini a O((V^3/3) * K)
# Algoritmo adattato da:
# https://www.geeksforgeeks.org/shortest-path-exactly-k-edges-directed-weighted-graph/
class WaveSelection3DMAtrix(SolverBase, DAGTopoSorting):
    def __init__(self, wavenet: WaveNetModel):
        super().__init__(wavenet)
        self.sp_matrix = None
        self.sp_prec = None
        self.u = None
        self.v = None

    def solve_model(self) -> None:
        import time

        self.sort_nodes(self.net)
        start = time.time()
        self._shortest_path(self.net.source, self.net.target)
        self.set_runtime(time.time() - start)

    def _shortest_path(self, source, target):
        V = len(self.topo_sorting)
        self.sp_matrix = np.zeros((V, V, self.k + 1))
        self.sp_matrix.fill(np.Inf)
        self.sp_prec = np.zeros((V, V, self.k + 1), dtype=int)
        self.sp_prec.fill(np.Inf)
        self.bicriterion = np.empty((V, V, self.k + 1), dtype=list)
        self.u = self.topo_sorting.index(source)
        self.v = self.topo_sorting.index(target)
        assert self.u <= self.v, ValueError("Nell'ordinamento il source non può essere dopo il target.")
        for e in range(self.k):
            for i in range(V):
                # TODO Possibile parallelizzare la ricerca
                for j in range(i, V):
                    self._process_matrix_cell(e, i, j, V)

        self._process_matrix_cell(self.k, self.u, self.v, V)

        if self.sp_matrix[self.u][self.v][self.k] == np.Inf:
            raise ValueError("MODEL INFEASIBLE")

    def _process_matrix_cell(self, e, i, j, V):
        # Per ogni cella tengo la lista dei percorsi possibili
        label_list = []
        # Il primo strato deve essere a zero per nodi su se stessi
        if e == 0 and i == j:
            self.sp_matrix[i][j][e] = 0
        if e == 1 and self.net.G.has_edge(self.topo_sorting[i], self.topo_sorting[j]):
            self.sp_matrix[i][j][e] = self.net.G[self.topo_sorting[i]][self.topo_sorting[j]]['weight']
            self.sp_prec[i][j][e] = j
            label_list.append((j, j, self.net.G[self.topo_sorting[i]][self.topo_sorting[j]]['weight']))
            self.bicriterion[i][j][e] = label_list

        # Esploro gli archi adiacenti solamente quando il
        # numero di archi è maggiore di 1
        if e > 1:
            for a in range(i, V):

                # Cerco un arco da i ad a e un cammino di lunghezza (e-1) da a ad j, in modo da unirli
                if (self.net.G.has_edge(self.topo_sorting[i], self.topo_sorting[a]) and i != a and
                        j != a and self.sp_matrix[a][j][e - 1] != np.Inf):
                    wght = self.net.G[self.topo_sorting[i]][self.topo_sorting[a]]['weight'] + \
                           self.sp_matrix[a][j][e - 1]
                    for idx, lb in enumerate(self.bicriterion[a][j][e - 1]):
                        lb_cost = lb[2] + self.net.G[self.topo_sorting[i]][self.topo_sorting[a]]['weight']
                        label_list.append((a, idx, lb_cost))
                    if self.sp_matrix[i][j][e] > wght:
                        self.sp_matrix[i][j][e] = wght
                        self.sp_prec[i][j][e] = a

            self.bicriterion[i][j][e] = label_list

    def get_path(self,):
        return self._matrix_to_path(self.u, self.v, self.k)

    def _matrix_to_path(self, s, t, lv):
        path = [self.topo_sorting[s], self.topo_sorting[self.sp_prec[s][t][lv]]]
        idx = self.sp_prec[s][t][lv]
        while lv > 1:
            lv -= 1
            path.append(self.topo_sorting[self.sp_prec[idx][t][lv]])
            idx = self.sp_prec[idx][t][lv]

        print(f"Path: {path}")
        return self._nodes_to_path(path)

    def obj_value(self) -> float:
        if self.sp_matrix[self.u][self.v][self.k] == np.Inf:
            return -1
        return self.sp_matrix[self.u][self.v][self.k]

    @staticmethod
    def log_filename():
        return "3d_waveselection_results"


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory
    import networkx as nx

    c = cf.TestConfig()
    netfactory = ONOCNetFactory(c)
    net = netfactory.make_wave_net(2, 2)

    model = WaveSelection3DMAtrix(net)
    model.solve_model()
    model.get_path()
    print(f"Cost: {model.obj_value()}")
    '''
    from numpy.linalg import matrix_power
    vediamo = model.sp_matrix[..., 1] # Seleziono il primo livello
    vediamo[vediamo == np.Inf] = 0
    vediamo[vediamo > 0] = 1

    dopo = matrix_power(vediamo, model.k)
    print(model.k)
    print(dopo[model.u][model.v])

    print(len([p for p in nx.shortest_simple_paths(net.G, source=net.source, target=net.target) if len(p) == 5]))

    print(len(model.bicriterion[model.u][model.v][model.k]))
    '''
    pass

