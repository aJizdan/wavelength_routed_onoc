from dynamicprogramming.solver_base import SolverBase
from dynamicprogramming.dag_topo_sorting import DAGTopoSorting
from graphs.wavenet_model import WaveNetModel
import numpy as np


# Metodo  per la ricerca dei cammini minimi di lunghezza K,
# la complessità in caso di un grafo complettamente connesso può
# raggiungere O(V^3*K). Possiamo sfruttare l'ordinamento topologico
# dei nodi per ridurre lo spazio di ricerca dei cammini a O((V^3/3) * K)
# Algoritmo adattato da:
# https://www.geeksforgeeks.org/shortest-path-exactly-k-edges-directed-weighted-graph/
class ShortestPathKEdges3DSorting(SolverBase, DAGTopoSorting):
    def __init__(self, wavenet: WaveNetModel):
        super().__init__(wavenet)
        self.route_matrix = None
        self.u = None
        self.v = None
        self.k = None

    def solve_model(self) -> None:
        import time
        # Edges in path = radii*resonances+1
        self.k = self.net.radii * self.net.resonances + 1
        self.sort_nodes(self.net)
        start = time.time()
        self._shortest_path(self.net.source, self.net.target)
        self.set_runtime(time.time() - start)

    def _shortest_path(self, source, target):
        V = len(self.topo_sorting)
        self.route_matrix = np.empty((V, V, self.k + 1), dtype=tuple)
        self.route_matrix.fill((None, np.Inf))
        self.u = self.topo_sorting.index(source)
        self.v = self.topo_sorting.index(target)
        assert self.u <= self.v, ValueError("Nell'ordinamento il source non può essere dopo il target.")
        for e in range(self.k):
            for i in range(V):
                # TODO Possibile parallelizzare la ricerca
                for j in range(i, V):
                    self._process_matrix_cell(e, i, j, V)

        self._process_matrix_cell(self.k, self.u, self.v, V)

        if self.route_matrix[self.u][self.v][self.k][1] == np.Inf:
            raise ValueError("MODEL INFEASIBLE")

    def _process_matrix_cell(self, e, i, j, V):
        # Il primo strato deve essere a zero per nodi su se stessi
        if e == 0 and i == j:
            self.route_matrix[i][j][e] = (None, 0)
        if e == 1 and self.net.G.has_edge(self.topo_sorting[i], self.topo_sorting[j]):
            self.route_matrix[i][j][e] = (j, self.net.G[self.topo_sorting[i]][self.topo_sorting[j]]['weight'])

        # Esploro gli archi adiacenti solamente quando il numero di archi è maggiore di 1
        if e > 1:
            for a in range(i, V):

                # Cerco un arco da i ad a e un cammino di lunghezza (e-1) da a ad j, in modo da unirli
                if (self.net.G.has_edge(self.topo_sorting[i], self.topo_sorting[a]) and i != a and
                        j != a and self.route_matrix[a][j][e - 1][1] != np.Inf):
                    wght = self.net.G[self.topo_sorting[i]][self.topo_sorting[a]]['weight'] + \
                           self.route_matrix[a][j][e - 1][1]
                    if self.route_matrix[i][j][e][1] > wght:
                        self.route_matrix[i][j][e] = (a, wght)

    def get_path(self, ):
        return self._matrix_to_path(self.u, self.v, self.k)

    def _matrix_to_path(self, s, t, lv):

        path = [self.topo_sorting[s], self.topo_sorting[self.route_matrix[s][t][lv][0]]]
        idx = self.route_matrix[s][t][lv][0]
        while lv > 1:
            lv -= 1
            path.append(self.topo_sorting[self.route_matrix[idx][t][lv][0]])
            idx = self.route_matrix[idx][t][lv][0]

        print(f"Path SP_k_3d_sorting: {path}")
        return self._nodes_to_path(path)

    def obj_value(self) -> float:
        if self.route_matrix[self.u][self.v][self.k][1] == np.Inf:
            return -1
        return self.route_matrix[self.u][self.v][self.k][1]

    @staticmethod
    def log_filename():
        return "3d_matrix_results"


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory

    c = cf.NetConfig()
    netfactory = ONOCNetFactory(c)
    net = netfactory.make_wave_net(4, 2)

    model = ShortestPathKEdges3DSorting(net)
    model.solve_model()
    model.get_path()
    print(f"Cost: {model.obj_value()}")
    pass

