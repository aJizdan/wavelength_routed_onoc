from dynamicprogramming.solver_base import SolverBase
from graphs.wavenet_model import WaveNetModel
from multiprocessing import Pool
import numpy as np
import os


# Metodo naive per la ricerca dei cammini minimi di lunghezza K,
# la complessità in caso di un grafo complettamente connesso può
# raggiungere O(V^K), dove V è il numero dei vertici. Anche se
# in questo caso è sempre minore, poichè il grafo è aciclico
# orientato e con un ordinamento. Algoritmo adattato da:
# https://www.geeksforgeeks.org/shortest-path-exactly-k-edges-directed-weighted-graph/
class ShortestPathKEdges(SolverBase):
    def __init__(self, wavenet: WaveNetModel):
        super().__init__(wavenet)

    def solve_model(self):
        import time
        start = time.time()
        k = self.net.radii * self.net.resonances + 1
        pool_size = 8
        params = []
        for edg in list(self.net.G.edges(self.net.source, data=True)):
            params.append((edg[1], self.net.target, k-1))
        assert len(params) > 0, ValueError("Il nodo source non è connesso al grafo.")
        if len(params) < pool_size:
            pool_size = len(params)
        # Creo una una worker queue per gli archi uscenti dal source
        with Pool(pool_size) as p:
            res = p.map(self._threaded_sh_path, params)
            res.sort()
            print(res)

        self.set_runtime(time.time() - start)

    def _threaded_sh_path(self, param):
        # print('module name:', __name__)
        # print('parent process:', os.getppid())
        # print('process id:', os.getpid())
        return self._shortest_path(param[0], param[1], param[2])

    def _shortest_path(self, source, target, k):
        if k == 0 and source == target:
            return 0
        if k == 1 and self.net.G.has_edge(source, target):
            return self.net.G[source][target]['weight']
        if k <= 0:
            return np.Inf

        res = np.Inf

        for edg in list(self.net.G.edges(source, data=True)):
            # Non ho bisogno di controllare che il nodo di arrivo dell'arco sia uguale al source
            if edg[1] != target:
                rec_res = self._shortest_path(edg[1], target, k-1)
                res = min(res, edg[2]['weight'] + rec_res)

        return res


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory
    import time

    c = cf.NetConfig()
    netfactory = ONOCNetFactory(c)

    net = netfactory.make_wave_net(2, 2)

    model = ShortestPathKEdges(net)

    model.solve_model()

    pass
