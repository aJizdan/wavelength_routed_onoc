from dynamicprogramming.solver_base import SolverBase
from graphs.wavenet_model import WaveNetModel
from multiprocessing import Pool, cpu_count
from typing import List


class YenNetXWaveSelection(SolverBase):
    def __init__(self, wavenet: WaveNetModel):
        super().__init__(wavenet)
        self.professional_sp_path = None

    def solve_model(self) -> None:
        import time
        import networkx as nx

        start = time.time()

        path_generator = filter(lambda path: len(path) == (self.k+1),
                                nx.shortest_simple_paths(self.net.G, self.net.source, self.net.target, weight='weight'))
        try:

            self.professional_sp_path = next(x for x in path_generator if
                                             self._compatible_routing_path(x))
            self.set_runtime(time.time() - start)
            '''
            with Pool(cpu_count()) as p:
                res = p.imap_unordered(self._return_path, path_generator, chunksize=16)
                for i in res:
                    print(i)
                    if i:
                        self.professional_sp_path = i
                        self.set_runtime(time.time() - start)
                        break
                p.close()
            '''
            if not self.professional_sp_path:
                raise ValueError
        except (ValueError, StopIteration, nx.exception.NetworkXNoPath):
            raise ValueError("MODEL INFEASIBLE")
        print(f"Path Yen_NX: {self.professional_sp_path}")

    def _return_path(self, path: List) -> List:
        if self._compatible_routing_path(path):
            return path
        return []

    def obj_value(self) -> float:
        if self.professional_sp_path is None:
            return -1
        professional_sp_edges = list(zip(self.professional_sp_path, self.professional_sp_path[1:]))
        return sum(map(lambda x: x[2]['weight'],
                       filter(lambda nd: (nd[0], nd[1]) in professional_sp_edges,
                              self.net.G.edges.data())))

    def get_path(self):
        return self._nodes_to_path(self.professional_sp_path)

    @staticmethod
    def log_filename():
        return "yen_enum_results"


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory

    c = cf.NetConfig(2)
    c.WAVEDATA = {'0': [1521, 1574],
                  '1': [1521, 1606]}
    c = cf.NetConfig()
    netfactory = ONOCNetFactory(c)
    net = netfactory.make_wave_net(4, 4)

    model = YenNetXWaveSelection(net)
    model.solve_model()
    pt = model.get_path()
    print(f"Cost: {model.obj_value()}")
    print(f"Cost wave_path: {pt.total_cost()}")
    pass
