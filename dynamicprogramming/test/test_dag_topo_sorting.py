import unittest
from dynamicprogramming.dag_topo_sorting import DAGTopoSorting


class TestDAGTopoSorting(unittest.TestCase):

    def test_topo_sorting_graph_same_node(self):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(6)
        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(8, 4)

        topo_soting = DAGTopoSorting()
        self.assertEqual(len(topo_soting.sort_nodes(net)), len(net.G.nodes()))

    def test_all_adjacent_nodes_follows(self):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.TestConfig()
        netfactory = ONOCNetFactory(c)
        net = netfactory.make_wave_net(2, 2)

        topo_soting = DAGTopoSorting()
        nodes_sorted = topo_soting.sort_nodes(net)

        for node in net.G.nodes():
            cut_index = nodes_sorted.index(node)
            self.assertTrue(set(net.G.adj[node]).issubset(nodes_sorted[(cut_index+1):]))

    def test_my_topo_soting_with_professional(self):
        import networkx as nx
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig()
        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(8, 4)

        topo_soting = DAGTopoSorting()
        my_sorting = topo_soting.sort_nodes(net)

        all_sortings = nx.all_topological_sorts(net.G)
        self.assertTrue(next((True for x in all_sortings if x == my_sorting), False))