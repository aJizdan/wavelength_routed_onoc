import unittest
from dynamicprogramming.shortest_path_k_edges3d_sorting import ShortestPathKEdges3DSorting
from dynamicprogramming.yen_netx_wave_selection import YenNetXWaveSelection


class TestYenNetworkXWaveSelection(unittest.TestCase):

    def test_shortest_path_is_the_shortest_path(self):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(4)
        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(4, 2)

        model = ShortestPathKEdges3DSorting(net)
        model.solve_model()

        my_path = model.get_path()

        professional_model = YenNetXWaveSelection(net)
        professional_model.solve_model()

        self.assertNotEqual(my_path.total_cost(), professional_model.obj_value())

    def test_routing_fault_checker(self):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(2)

        c.WAVEDATA = {'0': [1521, 1574],
                      '1': [1521, 1606]}

        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(2, 2)

        professional_model = YenNetXWaveSelection(net)
        with self.assertRaises(ValueError):
            professional_model.solve_model()
