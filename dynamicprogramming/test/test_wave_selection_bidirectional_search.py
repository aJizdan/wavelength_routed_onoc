import unittest
from dynamicprogramming.wave_selection_3d_matrix_routing_fault_parallel_look_ahead_dominance \
    import WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance
from dynamicprogramming.wave_selection_3d_matrix_bidirectional_search \
    import WaveSelection3DMatrixBidirectionalSearch


class TestShortestPathBidirectionalSearch(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(5)

        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(4, 1)

        cls.straight_model = WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance(net)
        cls.straight_model.solve_model()

        cls.reverse_net = netfactory.make_wave_net(4, 1, reverse=True)
        cls.bimodel = WaveSelection3DMatrixBidirectionalSearch(net, cls.reverse_net)

        cls.bimodel.solve_model()

    def test_bidirectional_and_one_way_search_have_same_results(self):
        """
        I percorsi di costo minimo restituiti sia nella ricerca one way che in quella bidirezionale devono
        essere gli stessi, poichè esaustive in entrambi i casi
        """
        min_lb = min(self.straight_model.bicriterion[self.straight_model.u][self.straight_model.v][self.straight_model.k], key=lambda t: t[2])
        index = self.straight_model.bicriterion[self.straight_model.u][self.straight_model.v][self.straight_model.k].index(min_lb)
        nodes_path = self.straight_model.matrix_label_to_nodes(self.straight_model.u, self.straight_model.v, self.straight_model.k, index)
        print(f"Optimal path: {nodes_path}")

        lbs_straight = [lb for lb in self.straight_model.bicriterion[self.straight_model.u][self.straight_model.v][self.straight_model.k]
               if lb[2] == min_lb[2]]
        print(lbs_straight)
        straight_tuples = set({})
        for num, lb in enumerate(lbs_straight):
            index = self.straight_model.bicriterion[self.straight_model.u][self.straight_model.v][
                self.straight_model.k].index(lb)
            nodes_path = self.straight_model.matrix_label_to_nodes(self.straight_model.u, self.straight_model.v,
                                                                   self.straight_model.k, index)
            straight_tuples.add(tuple(nodes_path))

        bituples = set({})
        for num, pt in enumerate(self.bimodel.sp_paths):
            bituples.add(tuple(pt))

        self.assertEqual(straight_tuples, bituples)

    def test_one_way_search_in_forward_and_backward_have_save_results(self):
        """
        Test della ricerca one way con reti diretta e inversa, queste devono dare gli stessi risultati
        """
        reverse_model = WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance(self.reverse_net)

        reverse_model.solve_model()

        min_lb = min(
            reverse_model.bicriterion[reverse_model.u][reverse_model.v][reverse_model.k],
            key=lambda t: t[2])

        lbs_reverse = [lb for lb in reverse_model.bicriterion[reverse_model.u][reverse_model.v][reverse_model.k]
                       if lb[2] == min_lb[2]]
        reverse_tuples = set({})
        for num, lb in enumerate(lbs_reverse):
            index = reverse_model.bicriterion[reverse_model.u][reverse_model.v][
                reverse_model.k].index(lb)
            nodes_path = reverse_model.matrix_label_to_nodes(reverse_model.u, reverse_model.v,
                                                             reverse_model.k, index)
            nodes_path.reverse()
            reverse_tuples.add(tuple(nodes_path))

        lbs_straight = [lb for lb in self.straight_model.bicriterion[self.straight_model.u][self.straight_model.v][
            self.straight_model.k] if lb[2] == min_lb[2]]

        straight_tuples = set({})
        for num, lb in enumerate(lbs_straight):
            index = self.straight_model.bicriterion[self.straight_model.u][self.straight_model.v][
                self.straight_model.k].index(lb)
            nodes_path = self.straight_model.matrix_label_to_nodes(self.straight_model.u, self.straight_model.v,
                                                                   self.straight_model.k, index)
            straight_tuples.add(tuple(nodes_path))

        self.assertEqual(len(lbs_reverse), len(lbs_straight))
        self.assertEqual(reverse_tuples, straight_tuples)