import unittest
from dynamicprogramming.wave_selection_3d_matrix_routing_fault_parallel import WaveSelection3DMatrixRoutingFaultParallel


class Test3DMatrixForParetoOptimal(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(4)
        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(2, 2)

        cls.model = WaveSelection3DMatrixRoutingFaultParallel(net)
        cls.model.solve_model()

        import numpy as np
        cls.lb_len = len(cls.model.bicriterion[cls.model.u][cls.model.v][cls.model.k])
        cls.all_paths = np.empty((cls.lb_len, cls.model.k + 1), dtype=list)
        for index in range(cls.lb_len):
            cls.all_paths[index] = cls.model._matrix_label_to_nodes(cls.model.u, cls.model.v, cls.model.k, index)

    def test_label_optimality(self):
        '''
        from collections import Counter

        for e in range(3, self.model.k):
            sliced_paths = list([tuple(sp) for sp in self.all_paths[:, (0, e)]])
            md_sliced_paths = list([tuple(sorted(sp)) for sp in self.all_paths[:, 1:e]])
            counter = Counter(sliced_paths)
            for morsetti, num in counter.items():
                md_collection = list([md_sliced_paths[idx] for idx, mr in enumerate(sliced_paths) if mr == morsetti])
                md_set = set(md_collection)
                with self.subTest(morsetti=morsetti, numero=num):
                    self.assertEqual(len(md_collection), len(md_set))

            # print(sliced_paths)
        '''