import unittest
from dynamicprogramming.shortest_path_k_edges3d_sorting import ShortestPathKEdges3DSorting


class TestShortestPathKEdges3DSroting(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(6)
        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(8, 4)

        cls.model = ShortestPathKEdges3DSorting(net)
        cls.model.solve_model()

    def test_shortest_path_matrix_plane_cutting(self):
        import numpy as np

        V = len(self.model.topo_sorting)
        for e in range(self.model.k + 1):
            for i in range(1, V):
                for j in range(i):
                    if i > j:
                        self.assertEqual(self.model.route_matrix[i][j][e][1], np.Inf)

    def test_shortest_path_matrix_counting(self):
        V = len(self.model.topo_sorting)
        num_full = 0
        num_optimized = 0
        for e in range(self.model.k + 1):
            for i in range(V):
                for j in range(V):
                    if i > j:
                        num_full += 1

        for e in range(self.model.k + 1):
            for i in range(1, V):
                for j in range(i):
                    num_optimized += 1

        self.assertEqual(num_full, num_optimized)

    def test_shortest_path_to_matrix_match_cost(self):
        path = self.model.get_path()
        self.assertEqual(path.total_cost(), self.model.obj_value())

    def test_shortest_path_is_the_shortest_path(self):
        import networkx as nx
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(4)
        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(4, 2)

        model = ShortestPathKEdges3DSorting(net)
        model.solve_model()

        my_path = model.get_path()

        path_generator = nx.shortest_simple_paths(model.net.G, model.net.source, model.net.target, weight='weight')
        # Trovo il primo cammino che sia della lunghezza giusta
        professional_sp_path = next(x for x in path_generator if len(x) == model.k+1)

        professional_sp_edges = list(zip(professional_sp_path, professional_sp_path[1:]))

        professional_sp_cost = sum(map(lambda x: x[2]['weight'],
                                       filter(lambda nd: (nd[0], nd[1]) in professional_sp_edges,
                                              model.net.G.edges.data())))

        self.assertEqual(my_path.total_cost(), professional_sp_cost)
