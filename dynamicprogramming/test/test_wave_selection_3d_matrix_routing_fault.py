import unittest
from dynamicprogramming.wave_selection_3d_matrix_routing_fault import WaveSelection3DMatrixRoutingFault


class TestShortestPathKEdges3DMAtrixAllPaths(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.TestConfig()
        c.WAVEDATA = {
            "0": [1496, 1574, 1601],
            "1": [1491, 1533, 1601],
        }

        netfactory = ONOCNetFactory(c)

        net = netfactory.make_wave_net(2, 2)

        cls.model = WaveSelection3DMatrixRoutingFault(net)
        cls.model.solve_model()

    def test_3d_matrix_finds_all_paths_help_with_networkx(self):
        import networkx as nx

        V = len(self.model.topo_sorting)
        for e in range(1, self.model.k):
            for i in range(V):
                for j in range(i, V):
                    if self.model.bicriterion[i][j][e]:
                        with self.subTest(i=i, j=j, e=e):
                            path_generator = nx.shortest_simple_paths(self.model.net.G, self.model.topo_sorting[i],
                                                                      self.model.topo_sorting[j], weight='weight')
                            # Trovo tutti i cammini che sia della profondità e
                            all_paths = set(list(map(tuple, filter(lambda x: len(x) == e+1 and
                                                                   self.model._compatible_routing_path(x),
                                                                   path_generator))))

                            matrix_paths = set([tuple(self.model._matrix_label_to_nodes(i, j, e, index))
                                                for index, x in enumerate(self.model.bicriterion[i][j][e])])

                            self.assertEqual(all_paths, matrix_paths)

        # Controllo l'ultimo livello
        path_generator = nx.shortest_simple_paths(self.model.net.G, self.model.net.source,
                                                  self.model.net.target, weight='weight')
        # Trovo tutti i cammini che sia della profondità della matrice
        all_paths = set(list(map(tuple, filter(lambda x: len(x) == self.model.k + 1 and
                                               self.model._compatible_routing_path(x),
                                               path_generator))))
        matrix_paths = set([tuple(self.model._matrix_label_to_nodes(self.model.u, self.model.v, self.model.k, index))
                           for index, x in enumerate(self.model.bicriterion[self.model.u][self.model.v][self.model.k])])
        self.assertEqual(all_paths, matrix_paths)
