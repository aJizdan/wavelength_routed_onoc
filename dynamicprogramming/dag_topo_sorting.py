from typing import List


# Ha la stessa complessità di DFS, cioè O(V+E)
# https://www.geeksforgeeks.org/topological-sorting/
class DAGTopoSorting:
    def __init__(self,):
        self.topo_sorting = []

    def sort_nodes(self, net) -> List:
        nodelist = list(net.G.nodes())
        for nd in nodelist:
            if nd not in self.topo_sorting:
                self._topological_sorting(nd, net)

        # print(f"Topo sorting: {self.topo_sorting}")
        assert set(nodelist) == set(self.topo_sorting), ValueError("Le due liste devono essere uguali.")
        assert len(nodelist) == len(self.topo_sorting), ValueError("Le due liste devono avere stessa dimensione.")

        return self.topo_sorting

    def _topological_sorting(self, source, net):
        for edg in list(net.G.edges(source, data=True)):
            if edg[1] not in self.topo_sorting and edg[1] != source:
                self._topological_sorting(edg[1], net)

        self.topo_sorting.insert(0, source)


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory

    c = cf.TestConfig()
    netfactory = ONOCNetFactory(c)

    network = netfactory.make_wave_net(2, 2)

    srt = DAGTopoSorting()
    print(srt.sort_nodes(network))
    pass

