from solver import Solver
from graphs.wavenet_model import WaveNetModel
from typing import List
from collections import Counter


class SolverBase(Solver):
    def __init__(self, wavenet: WaveNetModel) -> None:
        Solver.__init__(self, wavenet)
        self._runtime = None

        # Numero di archi nel cammino
        self.k = self.net.radii * self.net.resonances + 1

    def get_runtime(self) -> float:
        return self._runtime

    def set_runtime(self, runtime: float) -> None:
        print(f"Runtime: {runtime}")
        self._runtime = runtime

    def _no_routing_fault(self, path: List) -> bool:
        rays_in_path = set([self.net.G.nodes[x]['ray'] for x in path if self.net.G.nodes[x]['ray']])
        return self._intersect_ray_fault(path, rays_in_path)

    def _intersect_ray_fault(self, path: List, rays: set) -> bool:
        for nd in path:
            if len(rays.intersection(self.net.G.nodes[nd]['conflict_radii'])) > 0:
                return False

        return True

    def _compatible_routing_path(self, path: List) -> bool:
        rays_in_path = [x.split('_')[0] for x in path if x.split('_')[0].isdigit()]
        counter = Counter(rays_in_path)
        lv_x_ray = [v for k, v in counter.items()]
        if len(lv_x_ray) > self.net.radii:
            return False
        if max(lv_x_ray) > self.net.resonances:
            return False

        return self._intersect_ray_fault(path, set(rays_in_path))

    def _nodes_to_path(self, path):
        import networkx as nx
        from graphs.wavenet_path import WaveNetPath

        archi = list(zip(path, path[1:]))
        edges_in_path = list(filter(lambda nd: (nd[0], nd[1]) in archi, self.net.G.edges.data()))
        nodes_in_path = list(filter(lambda nd: nd[0] in path, self.net.G.nodes.data()))

        P = nx.DiGraph()
        P.add_nodes_from(nodes_in_path)
        P.add_edges_from(edges_in_path)
        assert nx.has_path(P, self.net.source, self.net.target), ValueError("Il grafo del cammino non è completo!")
        assert len(list(nx.all_simple_paths(P, self.net.source, self.net.target))) == 1, \
            ValueError("Il grafo deve avere un singolo cammino dal source al target!")

        # print(path)
        return WaveNetPath(self.net.radii, self.net.resonances, self.net.source, self.net.target, P)

    def get_path(self):
        pass

    def log_filename(self):
        pass

    def solve_model(self):
        pass
