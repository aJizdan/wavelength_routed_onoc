from dynamicprogramming.solver_base import SolverBase
from dynamicprogramming.dag_topo_sorting import DAGTopoSorting
from dynamicprogramming.wave_selection_3d_matrix_routing_fault_parallel_look_ahead_dominance \
    import WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance
from graphs.wavenet_model import WaveNetModel
import numpy as np


class WaveSelection3DMatrixBidirectionalSearch(SolverBase, DAGTopoSorting):
    def __init__(self, wavenet: WaveNetModel, reverse_wavenet: WaveNetModel):
        super().__init__(wavenet)
        self.u = None
        self.v = None

        self.forwardSearch = WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance(wavenet)
        self.backwardSearch = WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance(reverse_wavenet)

        self.sp_paths = None

    def solve_model(self) -> None:
        import time

        start = time.time()
        div_k = self.forwardSearch.k

        self.forwardSearch.k = div_k - div_k//2
        self.backwardSearch.k = div_k//2

        self.forwardSearch.solve_model(all_layers=True)
        self.backwardSearch.solve_model(all_layers=True)
        V = len(self.forwardSearch.topo_sorting) - 1

        costo = np.Inf
        for i in range(V):
            # Tutte le label dei percorsi da u a i
            labels = self.forwardSearch.bicriterion[self.forwardSearch.u][i][self.forwardSearch.k]
            if labels is not None and len(labels) > 0:
                # Trovo il corrispondente di i nella matrice di backwardSearch
                last_forward_node = self.forwardSearch.topo_sorting[i]
                join_node = self.backwardSearch.topo_sorting.index(last_forward_node)
                for id_lb, lb in enumerate(labels):
                    path_head = self.forwardSearch.matrix_label_to_nodes(self.forwardSearch.u, i, self.forwardSearch.k, id_lb)
                    # Qui processo le code
                    for id_lbb, lbb in enumerate(self.backwardSearch.bicriterion[self.backwardSearch.u][join_node][self.backwardSearch.k]):
                        path_tail = self.backwardSearch\
                            .matrix_label_to_nodes(self.backwardSearch.u, join_node, self.backwardSearch.k, id_lbb)
                        path_tail.reverse()
                        path_temp = path_head[:-1] + path_tail

                        if self.forwardSearch._compatible_routing_path(path_temp):
                            if lb[2]+lbb[2] < costo:
                                self.sp_paths = [path_temp]
                                costo = lb[2]+lbb[2]
                            elif lb[2]+lbb[2] == costo:
                                self.sp_paths.append(path_temp)

        self.set_runtime(time.time() - start)

        if self.sp_paths is None:
            self.set_obj_value(-1)
            raise ValueError("MODEL INFEASIBLE")

        self.set_obj_value(costo)

    def get_labels_number(self):
        return self.forwardSearch.get_labels_number() + self.backwardSearch.get_labels_number()

    @staticmethod
    def log_filename():
        return "3d_wavefault_bidirectional_search"

    def get_path(self,):
        return self._nodes_to_path(self.sp_paths[0])


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory
    # import pprofile

    c = cf.NetConfig(5)
    netfactory = ONOCNetFactory(c)
    # TODO eliminare gli archi che causano direttamente un conflitto
    net = netfactory.make_wave_net(4, 4)
    reverse_net = netfactory.make_wave_net(4, 4, reverse=True)
    print(f"Number of edges: {len(net.G.edges())}")
    # prof = pprofile.Profile()
    model = WaveSelection3DMatrixBidirectionalSearch(net, reverse_net)

    # with prof():
    model.solve_model()
    # prof.print_stats()
    pt = model.get_path()
    print(f"Cost: {model.obj_value()}")
    print(f"Wave_path: {pt}")
    print(f"Cost wave_path: {pt.total_cost()}")

    print(f"Numero labels: {model.get_labels_number()}")