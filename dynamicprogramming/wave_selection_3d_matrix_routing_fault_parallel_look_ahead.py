from dynamicprogramming.solver_base import SolverBase
from dynamicprogramming.dag_topo_sorting import DAGTopoSorting
from graphs.wavenet_model import WaveNetModel
import numpy as np
import networkx as nx
from multiprocessing import Pool, cpu_count
from typing import List
from collections import Counter


# Metodo  per la ricerca dei cammini minimi di lunghezza K,
# la complessità in caso di un grafo complettamente connesso può
# raggiungere O(V^3*K). Possiamo sfruttare l'ordinamento topologico
# dei nodi per ridurre lo spazio di ricerca dei cammini a O((V^3/3) * K)
# Algoritmo adattato da:
# https://www.geeksforgeeks.org/shortest-path-exactly-k-edges-directed-weighted-graph/
class WaveSelection3DMatrixRoutingFaultParallelLookAhead(SolverBase, DAGTopoSorting):
    def __init__(self, wavenet: WaveNetModel):
        super().__init__(wavenet)
        self.u = None
        self.v = None
        self.labels_number = 0
        self.oracle = None

    def solve_model(self) -> None:
        import time

        self.sort_nodes(self.net)
        self._oracle_look_ahead()
        start = time.time()
        self._shortest_path(self.net.source, self.net.target)
        self.set_runtime(time.time() - start)

    def _shortest_path(self, source, target):
        V = len(self.topo_sorting)
        self.bicriterion = np.empty((V, V, self.k + 1), dtype=list)
        self.u = self.topo_sorting.index(source)
        self.v = self.topo_sorting.index(target)
        assert self.u <= self.v, ValueError("Nell'ordinamento il source non può essere dopo il target.")

        for e in range(self.k):

            for i in range(V):
                # for j in range(i, V):
                self._process_matrix_cell(e, i, self.v, V)
            '''
            pool = Pool(3)
            result = pool.map(self._process_layer, [(e, i, j, V) for i in range(V) for j in range(i, V)])

            pool.close()
            pool.join()
            # print(result)
            for res in result:
                self.bicriterion[res[0]][res[1]][e] = res[2]
                self.labels_number += len(res[2])
            '''
            print(f"Finito di esaminare il livello: {e}")

        self._process_matrix_cell(self.k, self.u, self.v, V)

        if not self.bicriterion[self.u][self.v][self.k]:
            raise ValueError("MODEL INFEASIBLE")

    def _process_layer(self, param):
        return self._process_matrix_cell(*param)

    def _process_matrix_cell(self, e, i, j, V):
        # Per ogni cella tengo la lista dei percorsi possibili
        label_list = []
        id_i = self.topo_sorting[i]
        # Il primo strato deve essere a zero per nodi su se stessi
        if e == 0 and i == j:
            label_list.append((j, None, 0))
            self.bicriterion[i][j][e] = label_list
        if e == 1 and self.net.G.has_edge(id_i, self.topo_sorting[j]):
            if self._no_routing_fault([id_i, self.topo_sorting[j]]):
                label_list.append((j, None,
                                   self.net.G[id_i][self.topo_sorting[j]]['weight']))
            else:
                raise ValueError("Deve occuparsene il reparto graph factoring!")
            #     print(f"Found conflict: {[id_i, self.topo_sorting[j]]}")
            self.bicriterion[i][j][e] = label_list

        # Esploro gli archi adiacenti solamente quando il
        # numero di archi è maggiore di 1
        if e > 1:
            for a in range(i, V):
                # Cerco un arco da i ad a e un cammino di lunghezza (e-1) da a ad j, in modo da unirli
                # Il punto a cui mi aggancio per continuare il percorso deve esistere
                if i != a and j != a and self.bicriterion[a][j][e - 1] and \
                        self.net.G.has_edge(id_i, self.topo_sorting[a]):

                    wght = self.net.G[id_i][self.topo_sorting[a]]['weight']
                    for idx, lb in enumerate(self.bicriterion[a][j][e - 1]):
                        lb_cost = lb[2] + wght
                        # Qui devo decidere se la label va aggiunta oppure scartata perchè non rispetta
                        # criteri per la selezione dei raggi
                        lb_path = [id_i] + self._matrix_label_to_nodes(a, j, e-1, idx)

                        if self._compatible_routing_path(lb_path) and \
                                self._enough_look_ahead(i, j, lb_path):
                            label_list.append((a, idx, lb_cost))
                        else:
                            pass
                            # print("Conflict path: ",
                            # [id_i] + self._matrix_label_to_nodes(a, j, e-1, idx))

            self.bicriterion[i][j][e] = label_list

        return i, j, label_list

    def get_path(self,):
        min_lb = min(self.bicriterion[self.u][self.v][self.k], key=lambda t: t[2])
        index = self.bicriterion[self.u][self.v][self.k].index(min_lb)
        return self._nodes_to_path(self._matrix_label_to_nodes(self.u, self.v, self.k, index))

    def _matrix_label_to_nodes(self, s, t, lv, lb) -> List:
        assert lv >= 1, ValueError("Restituisco percrosi con almeno un arco.")

        path = [self.topo_sorting[s], self.topo_sorting[self.bicriterion[s][t][lv][lb][0]]]
        idx, lbx, cost = self.bicriterion[s][t][lv][lb]
        while lv > 1:
            lv -= 1
            path.append(self.topo_sorting[self.bicriterion[idx][t][lv][lbx][0]])
            idx, lbx, cost = self.bicriterion[idx][t][lv][lbx]

        # print(f"Path labeled: {path}")
        return path

    def _enough_look_ahead(self, i: int, j: int, lb_path: List,) -> bool:
        rays_in_path = [x.split('_')[0] for x in lb_path if x.split('_')[0].isdigit()]
        pt_counter = Counter(rays_in_path)

        tot_counter = Counter({x: self.net.resonances for x in pt_counter.keys()})

        enough = tot_counter - pt_counter

        rest_counter = self.oracle[i][0] + self.oracle[j][1]
        rest_counter.subtract(enough)
        if rest_counter.values() and min(rest_counter.values()) < 0:
            return False
        return True

    def _oracle_look_ahead(self):
        self.oracle = []
        for idx, node in enumerate(self.topo_sorting):
            descendants = nx.algorithms.dag.descendants(self.net.G, node)
            ancestors = nx.algorithms.dag.ancestors(self.net.G, node)

            descendants_ray = Counter([x.split('_')[0] for x in descendants if x.split('_')[0].isdigit()])
            ancestors_ray = Counter([x.split('_')[0] for x in ancestors if x.split('_')[0].isdigit()])

            self.oracle.insert(idx, (ancestors_ray, descendants_ray))

    def obj_value(self) -> float:
        if not self.bicriterion[self.u][self.v][self.k]:
            return -1
        return min([i[2] for i in self.bicriterion[self.u][self.v][self.k]])

    def get_labels_number(self):
        return self.labels_number

    @staticmethod
    def log_filename():
        return "3d_wavefault_parallel_results"


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory
    # import pprofile

    c = cf.NetConfig(5)
    netfactory = ONOCNetFactory(c)
    # TODO eliminare gli archi che causano direttamente un conflitto
    net = netfactory.make_wave_net(4, 1)
    # prof = pprofile.Profile()
    model = WaveSelection3DMatrixRoutingFaultParallelLookAhead(net)

    # with prof():
    model.solve_model()
    # prof.print_stats()
    pt = model.get_path()
    print(f"Cost: {model.obj_value()}")
    print(f"Cost wave_path: {pt.total_cost()}")

    print(f"Numero labels: {model.get_labels_number()}")
