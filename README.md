# Wavelength selection in Optical Networks on Chip
---
Per installare le librerie python necessarie in ambiente Linux usare:

```shell

$ pipenv install --three
$ pipenv shell
```
---
## Mixed integer linear programming

La directory **./ilpmodel** contiene i modelli PLI implementati con Gurobi e ORTools.

---

## Programmazione dinamica

La directory **./dynamicprogramming** contiene gli algoritmi di programmazione dinamica,
usando il label correcting. 
La versione finale è la classe **WaveSelection3DMatrixRoutingFaultParallelLookAheadDominance** nel 
corrispondente file.

---
## CLASP (ASP)

La directory **./asp** contiene la procedura di generazione dei dati e il codice Prolog.
Il file **./create_model_let.py** è usato per generare i dati.
Il file **./waveselection.pl** contiene il codice Prolog.

---