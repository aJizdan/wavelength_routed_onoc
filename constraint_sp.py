import logging
from bicriterion_base import BicriterionBase
from dijakstra_pq import print_path
import click
from label import Label
from heapq import *
from graphs.path import Path
from graphs.wavenet import WaveNet

logging.getLogger().setLevel(logging.INFO)


class ConstraintSP(BicriterionBase):
    """
    Usiamo un algoritmo di label correcting con una funzione multiobiettivo. Anche il criterio di dominanza di label
    su una sua laternativa viene valutato solamente in base al suo costo. Una ulteriore restrizione viene imposta
    sulle label che è possibile confrontare, queste devono aver attraversato lo stesso tipo e nello stesso numero
    i nodi che mappano i raggi. Elenchiamo i criteri di eliminazione di una label dal suo futuro sviluppo:
    1) Tra due label con lo stesso tipo e lo stesso numero di nodi attraversati viene scelto quello con il costo minore.
    2) Una label che ha superato il numero massimo di varietà di raggi viene eliminata.
    3) Una label che non ha abbastanza risonanze da completare il cammino davanti a se viene esclusa.
    """
    def __init__(self, wave_net: WaveNet):
        BicriterionBase.__init__(self, wave_net.G)
        self.wave_class = wave_net

    def search_paths(self, ):
        label_oid = 1
        dominating_paths: [Label] = []  # gamma
        labels_list: [Label] = []
        self.heap_push(labels_list, Label(label_oid, 0, self.source, None, 0, None))
        permanent_labels: [Label] = []

        while labels_list:
            current_label = self.heap_pop(labels_list)
            permanent_labels.append(current_label)

            # per ogni nodo estratto esamino i suoi nodi vicini
            vicini = self.wave_net.neighbors(current_label.nodo)
            for neighbor_key in vicini:
                neighbor_cost = current_label.cost + \
                                self.wave_net.get_edge_data(current_label.nodo, neighbor_key)[0]['weight']

                neighbor_trace = dict(current_label.ray_trace)
                if neighbor_key != self.target:
                    if self.wave_net.node[neighbor_key]['ray'] in neighbor_trace:
                        neighbor_trace[self.wave_net.node[neighbor_key]['ray']] += 1
                    else:
                        neighbor_trace[self.wave_net.node[neighbor_key]['ray']] = 1

                if self.check_rays(neighbor_trace) and self.look_ahead(neighbor_trace, neighbor_key):
                    label_oid += 1
                    new_label = Label(label_oid, neighbor_cost, neighbor_key,
                                      current_label.nodo, current_label.id, neighbor_trace)

                    # Prima di inserire la label in coda controllo se ci sono label con costo minore,
                    # questa ipotesi vale solo nodi con tipo di raggi e numero di risonanze uguali
                    if 'labels' not in self.wave_net.node[neighbor_key]:
                        self.heap_push(labels_list, new_label)
                    else:
                        to_delete = list(filter(lambda x: (x.ray_trace == neighbor_trace),
                                                self.wave_net.node[neighbor_key]['labels']))
                        if len(to_delete) > 0:
                            if to_delete[0].cost > new_label.cost:
                                self.remove_label(labels_list, to_delete[0])
                                # Lista delle label da esplorare
                                self.heap_push(labels_list, new_label)
                        else:
                            self.heap_push(labels_list, new_label)

            if current_label.nodo == self.target:
                # Aggiungo il cammino solo se rispetta il numero dei raggi e delle risonanze
                if self.check_completeness(current_label.ray_trace):
                    heappush(dominating_paths, current_label)
                    logging.info(f"Trovato potenziale cammino di costo: {current_label.cost}")

        logging.info(f"Esaminate {len(permanent_labels)} label.")
        if len(dominating_paths) == 0:
            return Path([], 0)
        return self.create_path(permanent_labels, heappop(dominating_paths))

    def check_rays(self, ray_trace):
        if len(list(ray_trace.keys())) <= self.wave_class.nradii:
            for ray in ray_trace.keys():
                if ray_trace[ray] > self.wave_class.nresonance:
                    return False
            return True
        else:
            return False

    def check_completeness(self, ray_trace):
        if len(ray_trace.keys()) != self.wave_class.nradii:
            return False

        for ray in ray_trace.keys():
            if ray_trace[ray] != self.wave_class.nresonance:
                return False
        return True

    def heap_pop(self, labels_list):
        label: Label = heappop(labels_list)
        self.wave_net.node[label.nodo]['labels'].remove(label)
        return label

    def heap_push(self, labels_list, label: Label):
        heappush(labels_list, label)
        if 'labels' in self.wave_net.node[label.nodo]:
            self.wave_net.node[label.nodo]['labels'].append(label)
        else:
            self.wave_net.node[label.nodo]['labels'] = [label]

    def remove_label(self, labels_list, label: Label):
        self.wave_net.node[label.nodo]['labels'].remove(label)
        labels_list.remove(label)

    def create_path(self, permanent_labels, target_label: Label):
        if target_label.nodo != self.target:
            raise ValueError(f"La label per la creazione del percorso non conincide con il target: {target_label.nodo}")

        shortest_path = Path([target_label.nodo], target_label.cost)
        prev_label_oid = target_label.prev_oid
        while prev_label_oid != 0:
            prev_label = next((lb for lb in permanent_labels if lb.id == prev_label_oid), None)
            shortest_path.nodes.insert(0, prev_label.nodo)
            prev_label_oid = prev_label.prev_oid

        return shortest_path

    def look_ahead(self, trace, node):
        for ray in trace.keys():
            if self.wave_net.node[node]['resources'][ray] < self.wave_class.nresonance - trace[ray]:
                return False

        return True


@click.command()
@click.option('--limit_radii', default=50, help='Numero di raggi da inserire nel grafo.')
@click.option('--nodes_in_radii', default=120, help='Numero di nodi per ogni raggio.')
@click.option('--tau', default=37, help='Peso massimo che un arco può raggiungere.')
def constraint_path(limit_radii, nodes_in_radii, tau):
    from graphs.wavenet import WaveNet
    from graphs.risonanze import DATA, SOURCE, TARGET

    wavelength_net = WaveNet(DATA, SOURCE, TARGET, nradii=4, nresonance=2)
    wavelength_net.select_rays(limit_radii=limit_radii, num_nodes_per_radii=nodes_in_radii)
    wavelength_net.create_graph(tau=tau, )
    wavelength_net.add_routing_fault_radii()
    wavelength_net.add_look_ahead()

    csp = ConstraintSP(wavelength_net)
    try:
        cs_path = csp.search_paths()
    except Exception as ex:
        click.secho(str(ex), fg='red', bold=True)
        return

    print_path(cs_path)


if __name__ == '__main__':
    constraint_path()
