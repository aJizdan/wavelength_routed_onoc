from graphs.risonanze import SOURCE, TARGET


def delta_gap(nradii, nresonance):
    lambda_min = SOURCE["freq"]
    lambda_max = TARGET["freq"]
    return (lambda_max - lambda_min) / (nresonance * nradii - 1)


def upper_tau(nradii, nresonance):
    lambda_min = SOURCE["freq"]
    lambda_max = TARGET["freq"]
    return lambda_max - lambda_min - delta_gap(nradii, nresonance)