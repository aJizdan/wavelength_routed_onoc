from abc import ABC, abstractmethod
from graphs.risonanze import Config


# The Abstract Net Factory:
class NetFactory(ABC):
    def __init__(self, config: Config):
        self.config = config

    @abstractmethod
    def make_wave_net(self, radii, resonances, tau=None):
        pass
