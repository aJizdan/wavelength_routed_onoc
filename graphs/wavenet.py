import networkx as nx
from graphs.path import Path
import itertools as it
# Si occupa della creazione del grafo come lista di adiacenze, con metodi integrati per il disegno
# schematico e salvataggio in png


class WaveNet:
    def __init__(self, DATA, SOURCE, TARGET, nradii=2, nresonance=2):
        # Dati delle risonanze e del nodo inizile e finale
        self.DATA = DATA
        self.SOURCE = SOURCE
        self.TARGET = TARGET

        self.nradii = nradii
        self.nresonance = nresonance
        # Seleziono i raggi da un file esterno
        self.rays = None
        
        # immutable vars
        self._lambda_min = SOURCE["freq"]
        self._lambda_max = TARGET["freq"]

        from graphs.util import delta_gap, upper_tau
        self.delta = delta_gap(nradii, nresonance)
        self.tau_max = upper_tau(nradii, nresonance)

        self.G: nx.DiGraph = None

        # vincolo sulla risorsa, devo scegliere esattamente #nresonance per ogni radii
        self.resources = None
        self.graph = None
        self.nodes = None
        self.avilable_resources = None
        self.edges = None
        self.tau = None
        self.radii = None

    def select_rays(self, limit_radii=None, num_nodes_per_radii=None):
        # prendo dai dati i primi #nradii array di risonanze
        rays_key_list = list(self.DATA.keys())

        if limit_radii is not None:
            rays_key_list = rays_key_list[:limit_radii]

        selected_rays = [self.DATA.get(k) for k in rays_key_list]
        if num_nodes_per_radii is None:
            self.rays = selected_rays
            return

        self.rays = [self.equi_list_slicer(lr, num_nodes_per_radii) for lr in selected_rays]

    def equi_list_slicer(self, lambda_list, num_lambda):
        if len(lambda_list) <= num_lambda:
            return lambda_list

        segment_len = int(len(lambda_list)//num_lambda)
        index = 0
        result = []
        while index < len(lambda_list) and len(result) != num_lambda:
            result.append(lambda_list[index])
            index += segment_len

        return result

    def create_graph(self, tau=20):
        self.tau = tau
        self.G = nx.DiGraph()
        self.radii = []

        for key, value in enumerate(self.rays):
            self.radii.append(f"{key}")
            for radius in value:
                self.G.add_node(f"{key}_{radius}", ray=str(key), radius=radius)

        comb = it.combinations(self.G.nodes.data(), 2)
        only_positives_edges = it.filterfalse(lambda nd: nd[0][1]['radius'] >= nd[1][1]['radius'], comb)
        edges_list = map(lambda edg: (edg[0][0], edg[1][0],
                                          self.costo_lambda_to_lambda(edg[0][1]['radius'], edg[1][1]['radius'])),
            only_positives_edges)

        tau_cost_limit = filter(lambda edg: edg[2] < tau, edges_list)

        self.G.add_weighted_edges_from(tau_cost_limit)

        # Aggiungo nodo iniziale e finale al grafo
        # self.G.add_node(self.SOURCE["label"], ray="s", radius=self.SOURCE["freq"])
        # self.G.add_node(self.TARGET["label"], ray="t", radius=self.TARGET["freq"])
        source_nd = (self.SOURCE["label"], "s", self.SOURCE["freq"])
        target_nd = (self.TARGET["label"], "t", self.TARGET["freq"])

        # Aggiunto gli archi per i nodi fittizi
        nds_out_source = map(lambda nd: (source_nd[0], nd[0], nd[1]['radius'] - source_nd[2]), self.G.nodes.data())
        tau_cost_src = list(filter(lambda edg: edg[2] < tau, nds_out_source))

        nds_in_target = map(lambda nd: (nd[0], target_nd[0], target_nd[2] - nd[1]['radius']), self.G.nodes.data())
        tau_cost_tgt = list(filter(lambda edg: edg[2] < tau, nds_in_target))

        self.G.add_weighted_edges_from(tau_cost_src)
        self.G.add_weighted_edges_from(tau_cost_tgt)
        self.G.node[source_nd[0]]['ray'] = ''
        self.G.node[target_nd[0]]['ray'] = ''
        self.G.node[source_nd[0]]['radius'] = source_nd[2]
        self.G.node[target_nd[0]]['radius'] = target_nd[2]

    def add_cost_attributes(self):
        import numpy as np

        nx.set_node_attributes(self.G, np.inf, 'cost')
        nx.set_node_attributes(self.G, None, 'prev')

    def add_look_ahead(self):
        nx.set_node_attributes(self.G, None, 'resources')

        for node in self.G.nodes:
            res = dict({})
            for key, val in enumerate(self.rays):
                # Routing Fault:
                if str(key) in self.G.node[node]['conflict_radii']:
                    res[key] = -1
                else:
                    res[key] = len(list(filter(lambda x: x > self.G.node[node]["radius"], val)))
            self.G.node[node]['resources'] = res

    def add_routing_fault_radii(self):

        for node in self.G.nodes.data():
            fault_resonances = list(filter(lambda nd: nd[0] != node[0] and
                                                  nd[1]['radius'] == node[1]['radius'] and
                                                  nd[1]['ray'] in self.radii and
                                                  node[1]['ray'] in self.radii,
                                    self.G.nodes.data()))
            self.G.node[node[0]]['conflict_resonances'] = list(map(lambda x: x[0], fault_resonances))
            self.G.node[node[0]]['conflict_radii'] = list(map(lambda x: x[1]['ray'], fault_resonances))


    def check_required_path(self, path: Path):
        if len(path.nodes) != (self.nradii*self.nresonance)+2:
            return False

        check_ray = path.find_rays()

        if len(check_ray.keys()) != self.nradii:
            return False

        for k in check_ray.keys():
            if len(check_ray[k]) != self.nresonance:
                return False
            # Mi assicuro in caso di selezione di un raggio con un picco avente
            # un lista di altri raggi in conflitto, questi non venogno scelti
            for n in check_ray[k]:
                radii_list = list(check_ray.keys())
                radii_list.remove(k)
                if len(list(set(self.G.node[n]['conflict_radii']) & set(radii_list))) > 0:
                    return False

        return True

    # Gli archi, da lambda_i a lambda_j hanno costo abs(delta - (lambda_j-lambda_i))
    def costo_lambda_to_lambda(self, lambda_i, lambda_j):
        return abs(self.delta - (lambda_j - lambda_i))

    def model_resource(self, grph):
        resource = {}
        for nd in grph.keys():
            if nd[:2] not in resource and nd[:2] != 's_' and nd[:2] != 't_':
                resource[nd[:2]] = self.nresonance

        assert len(resource.keys()) == self.nradii, "Il numero di risorse diverso da numero di raggi."

        resource['t_'] = 0
        resource['s_'] = 0
        return resource

    def look_ahead_resource(self):
        self.avilable_resources = {}
        for etc in self.graph.keys():
            picco = int(etc[2:])
            res_available = dict.fromkeys(self.resources, 0)
            for nd in [item for sublist in self.nodes for item in sublist]:
                if picco < nd[1]:
                    suffix_res = nd[0][:2]
                    res_available[suffix_res] += 1

            self.avilable_resources[etc] = res_available

    def is_ahead_exists_resources(self, key, current_res):
        if key[:2] != 's_' and key[:2] != 't_':
            for k, risorse_disponibili in self.avilable_resources[key].items():
                if current_res[k] > risorse_disponibili:
                    return False

        return True


if __name__ == "__main__":
    from graphs.risonanze import DATA, SOURCE, TARGET
    wavelength_net = WaveNet(DATA, SOURCE, TARGET)
    wavelength_net.create_graph(tau=119)
    # wavelength_net.plot_graph(edges, nodes, graph, show_and_save=True)

