import unittest


class TestWavenetModel(unittest.TestCase):
    def setUp(self):
        self.nrays = 8
        self.peaks = 8
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig()
        self.netfactory = ONOCNetFactory(c)
        self.network = self.netfactory.make_wave_net(self.nrays, self.peaks)

    def test_existence_node_conflict_resonances_attributes(self):
        for nd in self.network.G.nodes.data():
            self.assertIn('conflict_resonances', nd[1])

    def test_existence_node_conflict_radii_attributes(self):
        for nd in self.network.G.nodes.data():
            self.assertIn('conflict_radii', nd[1])

    def test_conflict_radii_peasks_contain_node_resonance(self):
        for nd in self.network.G.nodes.data():
            for rd in nd[1]['conflict_radii']:
                with self.subTest(node=nd[0], radius=rd):
                    filter_nodes = list(filter(lambda x: x[1]['ray'] == rd, self.network.G.nodes.data()))
                    peaks = list(map(lambda x: x[1]['radius'], filter_nodes))

                    self.assertIn(nd[1]['radius'], peaks)

    def test_not_conflict_radii_peaks_not_contain_node_resonance(self):
        net_radii = self.network.get_net_radii()
        for nd in self.network.G.nodes.data():
            if nd[0] != self.network.source and nd[0] != self.network.target:
                filter_nodes = list(filter(lambda x: x[1]['ray'] not in nd[1]['conflict_radii'] and
                                                     x[1]['ray'] != nd[1]['ray'] and
                                                     x[1]['ray'] in net_radii,
                                           self.network.G.nodes.data()))
                peaks = list(map(lambda x: x[1]['radius'], filter_nodes))
                with self.subTest(node=nd[0]):
                    self.assertNotIn(nd[1]['radius'], peaks)

    def test_no_edges_between_conflict_node_and_rays(self):
        for node in self.network.G.nodes.data():
            if node[1]['conflict_radii']:

                fault_node_successors = list(filter(lambda n:
                                                    self.network.G.node[n]['ray'] in node[1]['conflict_radii'],
                                                    self.network.G.successors(node[0])))
                for nd in fault_node_successors:
                    self.assertFalse(self.network.G.has_edge(node[0], nd))

                fault_node_predecessors = list(filter(lambda n:
                                                      self.network.G.node[n]['ray'] in node[1]['conflict_radii'],
                                                      self.network.G.predecessors(node[0])))
                for nd in fault_node_predecessors:
                    self.assertFalse(self.network.G.remove_edge(nd, node[0]))