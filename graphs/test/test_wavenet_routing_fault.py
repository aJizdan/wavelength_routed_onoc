import unittest


class TestWavenetModel(unittest.TestCase):
    def setUp(self):
        self.nrays = 4
        self.peaks = 1
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig(5)
        self.netfactory = ONOCNetFactory(c)
        self.network = self.netfactory.make_wave_net(self.nrays, self.peaks)

    def test_no_edges_to_routing_fault_node(self):
        print(f"Numero nodi: {len(self.network.G.nodes())}")

        for edg in self.network.G.edges.data():
            with self.subTest(edg=edg, msg='Test stella uscente'):
                source_ray = self.network.G.nodes[edg[0]]['ray']
                conflict_target = self.network.G.nodes[edg[1]]['conflict_radii']

                self.assertNotIn(source_ray, conflict_target)

            with self.subTest(edg=edg, msg='Test stella entrante'):
                source_ray = self.network.G.nodes[edg[1]]['ray']
                conflict_target = self.network.G.nodes[edg[0]]['conflict_radii']

                self.assertNotIn(source_ray, conflict_target)