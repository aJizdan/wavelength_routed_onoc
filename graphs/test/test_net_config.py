import unittest
from graphs.risonanze import NetConfig, Config


class TestNetConfig(unittest.TestCase):

    def test_wavedata_size_match_lenght_of_wave_data(self):
        size = 5
        c = NetConfig(size)
        self.assertEqual(len(list(c.WAVEDATA.keys())), size)

    def test_wavedata_netconfig_match_lenght_of_wave_data_config(self):
        nc = NetConfig()
        c = Config()
        self.assertGreaterEqual(len(list(c.WAVEDATA.keys())), len(list(nc.WAVEDATA.keys())))

    def test_wavedata_netconfig_not_zero_or_below(self):
        with self.assertRaises(AssertionError):
            nc = NetConfig(0)
