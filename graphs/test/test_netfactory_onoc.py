import unittest


class TestNetworkFactoryConfigFile(unittest.TestCase):
    def setUp(self):
        self.cases = list(zip([4, 8]*3, [1, 4, 8]*2))
        self.cases.sort()
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory

        c = cf.NetConfig()
        self.netfactory = ONOCNetFactory(c)

    def test_existence_node_ray_attributes(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                for nd in network.G.nodes.data():
                    self.assertIn('ray', nd[1])

    def test_existence_node_peak_attributes(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                for nd in network.G.nodes.data():
                    self.assertIn('radius', nd[1])

    def test_node_peak_attributes_boundaries(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                for nd in network.G.nodes.data():
                    self.assertTrue(self.netfactory.config.LAMBA_MIN <= nd[1]['radius']
                                    <= self.netfactory.config.LAMBA_MAX)

    def test_existence_edge_weight_attributes(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                for edg in network.G.edges.data():
                    self.assertIn('weight', edg[2])

    def test_weight_edge_greater_or_equal_zero(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                for edg in network.G.edges.data():
                    self.assertGreaterEqual(edg[2]['weight'], 0)

    def test_source_node_in_degree_equal_zero(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                self.assertEqual(network.G.in_degree(self.netfactory.config.SOURCE_NODE[0]), 0)

    def test_taget_node_out_degree_equal_zero(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                self.assertEqual(network.G.out_degree(self.netfactory.config.TARGET_NODE[0]), 0)

    def test_weight_edge_smaller_than_tau(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):
                tau = 30
                network = self.netfactory.make_wave_net(nrays, pll, tau=tau)
                for edg in network.G.edges.data():
                    self.assertGreater(tau, edg[2]['weight'])

    def test_no_edge_from_source_to_target(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):
                network = self.netfactory.make_wave_net(nrays, pll)
                self.assertFalse(network.G.has_edge(self.netfactory.config.SOURCE_NODE[0],
                                                    self.netfactory.config.TARGET_NODE[0]))

    def test_no_edge_per_nodes_with_same_peak(self):
        for nrays, pll in self.cases:
            with self.subTest(nrays=nrays, pll=pll):

                network = self.netfactory.make_wave_net(nrays, pll)
                for edg in network.G.edges.data():
                    if edg[0] != self.netfactory.config.SOURCE_NODE[0] and edg[1] != self.netfactory.config.TARGET_NODE[0]:
                        self.assertNotEqual(network.G.node[edg[0]]['radius'], network.G.node[edg[1]]['radius'])
