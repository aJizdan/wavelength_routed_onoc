from abc import ABC
import networkx as nx


class WaveNetBase(ABC):
    def __init__(self, radii: int, resonances: int, source: str, target: str, graph):
        assert radii > 0, ValueError('Il numero di raggi deve essere maggiore di zero.')
        assert resonances > 0, ValueError('I numero di risonanze da selezionare deve essere maggiore di zero.')
        self.radii = radii
        self.resonances = resonances
        self.G: nx.DiGraph = graph
        self.source = source
        self.target = target

    def has_source_to_target_path(self):
        return nx.has_path(self.G, self.source, self.target)