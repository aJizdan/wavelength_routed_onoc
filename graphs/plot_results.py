import matplotlib.pyplot as plt

# Plot dei risultati per esplorazione dei sotto-grafi, cotruiti in base a diversi valori
# del parametro tau


def plot_tau_results(x, y, n=None, queue=''):
    plt.plot(x, y, label='linear', marker='x')
    plt.title(r"Cammini trovati per distanza $\tau$." + f"Coda {queue}")
    plt.xlabel(r'$\tau$')
    plt.ylabel('#cammini')
    # plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
    # plt.legend()
    plt.grid(True)
    '''
    fig, ax = plt.subplots()
    ax.scatter(x, y)

    for i, txt in enumerate(n):
        ax.annotate(txt, (x[i], y[i]))
    '''
    plt.show()
    pass
