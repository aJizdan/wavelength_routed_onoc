import os
import matplotlib.pyplot as plt
import networkx as nx
from networkx.readwrite import json_graph


def show_graph(G: nx.MultiGraph, show_and_save=False, dest='.'):
    pos = nx.spring_layout(G)


def plot_graph(edges, nodes, grph, show_and_save=False, dest='.'):
    G = nx.DiGraph()
    pos = nx.spring_layout(G)

    for i, node_list_ray in enumerate(nodes):
        G.add_nodes_from([nd[0] for nd in node_list_ray])
        for node in node_list_ray:
            pos[node[0]] = node[2]
        nx.draw_networkx_nodes(G, pos,
                               nodelist=[nd[0] for nd in node_list_ray],
                               node_color=range(len(node_list_ray)),
                               cmap=list(plt.cm.cmap_d.keys())[ i *3],
                               node_size=400,
                               alpha=0.8)

    for ed in edges:
       G.add_edge(ed[0], ed[1])  # , weight=3)

    nx.draw_networkx_labels(G, pos, font_size=6)
    nx.draw_networkx_edges(G, pos, edge_list=edges, arrows=True, width=1.0, alpha=0.5)
    # nx.draw_networkx_edge_labels(G, pos, font_size=2)

    # plt.figure(dpi=1200)

    for nd, ps in pos.items():
        overlap = set_ovelapping_wavelngths(nd, grph)
        if overlap:
            plt.axvline(x=ps[0], alpha=0.2, linewidth=4, color='r')

    plt.axis('off')
    if show_and_save:
        plt.savefig(os.path.join(dest, "grafo"), dpi=300)  # save as png, dpi for high quality
        # plt.savefig("grafo.svg")  # save as svg
    plt.show()  # display


def set_ovelapping_wavelngths(node, grph):
    overlap = []
    for key in grph.keys():
        if key != node and key[2:] == node[2:]:
            overlap.append(key)

    return overlap
