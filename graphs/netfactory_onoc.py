from graphs.wavenet_model import WaveNetModel
from graphs.netfactory_base import NetFactory


# The Abstract Net Factory:
class ONOCNetFactory(NetFactory):
    def __init__(self, config):
        super().__init__(config)

    def make_wave_net(self, radii, resonances, tau=None, limit_radii=None, reverse=False):
        delta = self.delta_gap(radii, resonances)

        tau_max = self.upper_tau(delta)
        if tau is None:
            tau = tau_max
        if tau > tau_max:
            raise ValueError("Il TAU indicato non deve superare il TAU_MAX")

        assert tau > 0, ValueError('Il limite per il costo degli archi deve essere maggiore di zero.')
        graph = self._create_wave_graph(delta, tau, limit_radii)
        if not reverse:
            source = self.config.SOURCE_NODE[0]
            target = self.config.TARGET_NODE[0]
            return WaveNetModel(radii, resonances, source, target, delta=delta, tau=tau, graph=graph)

        target = self.config.SOURCE_NODE[0]
        source = self.config.TARGET_NODE[0]
        reverse_graph = graph.reverse()
        return WaveNetModel(radii, resonances, source, target, delta=delta, tau=tau, graph=reverse_graph)

    # Calcolo la distanza ideale tra due picchi successivi
    def delta_gap(self, radii, resonances):
        return (self.config.LAMBA_MAX - self.config.LAMBA_MIN) / (resonances * radii - 1)

    # Il limite superiore per il costo degli archi
    def upper_tau(self, delta):
        return self.config.LAMBA_MAX - self.config.LAMBA_MIN - delta

    def _create_wave_graph(self, delta, tau, limit_radii):
        import networkx as nx
        import itertools as it
        G = nx.DiGraph()

        for key, lambdas in self.config.WAVEDATA.items():
            # print(f"{key} e {lambdas}")
            assert len(lambdas) > 0, ValueError("Per ogni raggio deve essere disponibile almeno una risonanza.")
            G.add_nodes_from(list(map(lambda radius: (f"{key}_{radius}", {"ray": key, "radius": radius}), lambdas)))

        source_nd = (self.config.SOURCE_NODE[0], {"ray": "", "radius": self.config.SOURCE_NODE[1]})
        target_nd = (self.config.TARGET_NODE[0], {"ray": "", "radius": self.config.TARGET_NODE[1]})

        permutation = it.permutations(G.nodes.data(), 2)
        only_positives_edges = it.filterfalse(lambda nd: nd[0][1]['radius'] >= nd[1][1]['radius'], permutation)
        edges_list = map(lambda edg: (edg[0][0], edg[1][0],
                                      abs(delta - (edg[1][1]['radius'] - edg[0][1]['radius']))),
                         only_positives_edges)

        tau_cost_limit = filter(lambda edg: edg[2] < tau, edges_list)

        G.add_weighted_edges_from(tau_cost_limit)

        # Aggiunto gli archi per i nodi fittizi
        nds_out_source = map(lambda nd: (source_nd[0], nd[0], nd[1]['radius'] - source_nd[1]['radius']), G.nodes.data())
        tau_cost_src = list(filter(lambda edg: edg[2] < tau, nds_out_source))

        nds_in_target = map(lambda nd: (nd[0], target_nd[0], target_nd[1]["radius"] - nd[1]['radius']), G.nodes.data())
        tau_cost_tgt = list(filter(lambda edg: edg[2] < tau, nds_in_target))

        G.add_nodes_from([source_nd, target_nd])

        G.add_weighted_edges_from(tau_cost_src)
        G.add_weighted_edges_from(tau_cost_tgt)

        assert nx.is_directed_acyclic_graph(G), ValueError("Il grafo non è un DAG.")
        return G


if __name__ == "__main__":
    import graphs.risonanze as cf
    import networkx as nx
    from pygraphviz import *

    c = cf.NetConfig()
    c.WAVEDATA = {
        "0": [1496, 1509, 1521, 1547, 1574, 1588, 1601],
        "1": [1499, 1510, 1521, 1533, 1557, 1569, 1606],
    }
    netfactory = ONOCNetFactory(c)

    net = netfactory.make_wave_net(2, 2,tau=30)
    A = nx.nx_agraph.to_agraph(net.G)
    A.draw('star.png', prog='circo')  # draw to png using circo
    pass
