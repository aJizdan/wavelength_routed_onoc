import networkx as nx
from graphs.wavenet_base import WaveNetBase


class WaveNetPath(WaveNetBase):
    def __init__(self, nradii, nresonance, source: str, target: str, graph: nx.DiGraph):
        WaveNetBase.__init__(self, nradii, nresonance, source, target, graph=graph)

    def total_cost(self):
        return sum(map(lambda x: x[2]['weight'], self.G.edges.data()))

    def __str__(self):
        return str(nx.shortest_path(self.G, self.source, self.target))
