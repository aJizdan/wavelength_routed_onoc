from graphs.wavenet_base import WaveNetBase
import networkx as nx


class WaveNetModel(WaveNetBase):
    def __init__(self, nradii, nresonance, source: str, target: str, delta, tau, graph: nx.DiGraph):
        assert delta > 0, ValueError('La ditanza ideale tra le lunghezza d\'onda deve essere maggiore di zero.')
        assert tau > 0, ValueError('Il limite per il costo degli archi deve essere maggiore di zero.')
        WaveNetBase.__init__(self, nradii, nresonance, source, target, graph=graph)
        self.delta = delta
        self.tau = tau
        self._add_conflict_radii()

    def get_net_radii(self):
        filtered_nodes = filter(lambda nd: nd[0] != self.source and nd[0] != self.target, self.G.nodes.data())
        return self._collect_rays(filtered_nodes)

    def _add_conflict_radii(self):
        import itertools as it

        filtered_nodes = list(filter(lambda nd: nd[1]['ray'], self.G.nodes.data()))
        permutation = it.permutations(filtered_nodes, 2)
        conflict_edges = list(filter(lambda nd: nd[0][1]['radius'] == nd[1][1]['radius'], permutation))

        nx.set_node_attributes(self.G, list([]), 'conflict_resonances')
        nx.set_node_attributes(self.G, list([]), 'conflict_radii')
        for node in filtered_nodes:
            conflict_edges_in_node = list(filter(lambda edg: node[0] == edg[0][0], conflict_edges))
            if conflict_edges_in_node:
                self.G.node[node[0]]['conflict_resonances'] = list(map(lambda x: x[1][0], conflict_edges_in_node))
                fault_rays = list(map(lambda x: x[1][1]['ray'], conflict_edges_in_node))
                self.G.node[node[0]]['conflict_radii'] = fault_rays

                fault_node_successors = list(filter(lambda n: self.G.node[n]['ray'] in fault_rays,
                                                    self.G.successors(node[0])))
                for nd in fault_node_successors:
                    self.G.remove_edge(node[0], nd)
                fault_node_predecessors = list(filter(lambda n: self.G.node[n]['ray'] in fault_rays,
                                                      self.G.predecessors(node[0])))
                for nd in fault_node_predecessors:
                    self.G.remove_edge(nd, node[0])

    @staticmethod
    def _collect_rays(nodes):
        radii_list = map(lambda nd: nd[1]['ray'], nodes)
        return set(radii_list)
