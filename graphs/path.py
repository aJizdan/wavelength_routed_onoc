class Path:
    def __init__(self, nodes: [str], cost: int, runtime=0):
        self.nodes = nodes
        self.cost = cost
        self.runtime = runtime

    def __eq__(self, other):
        return self.nodes == other.nodes

    def __lt__(self, other):
        return self.cost < other.cost

    def find_rays(self):
        check_ray = {}
        for nd in self.nodes[1:-1]:
            r = nd.split('_')[0]
            if r not in check_ray:
                check_ray[r] = [nd]
            else:
                check_ray[r].append(nd)

        return check_ray

