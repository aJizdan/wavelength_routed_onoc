from numpy import genfromtxt

import json
datastore = {}
# Writing JSON data
with open('csvjson.json', 'r') as f:
    datastore = json.load(f)

edges = list(map(lambda x: (x['index #1'], x['index #2']), datastore))

print(len(edges))

edges_set = set(edges)
print(len(edges_set))

import graphs.risonanze as cf
from graphs.netfactory_onoc import ONOCNetFactory
# import pprofile

c = cf.NetConfig(5)
netfactory = ONOCNetFactory(c)
# TODO eliminare gli archi che causano direttamente un conflitto
net = netfactory.make_wave_net(4, 1)
print(f"Number of edges: {len(net.G.edges())}")

gr_edges = list(map(lambda x: (x[0], x[1]), net.G.edges()))

print(len(gr_edges))
gr_edges_set = set(gr_edges)
print(len(gr_edges_set))

for edg in edges_set.difference(gr_edges_set):
    print(f"{edg} present: {net.G.has_edge(edg[0], edg[1])}")