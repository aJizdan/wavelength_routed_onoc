import copy
from heapq import *
import click

from dijakstra_pq import DijakstraPQ, print_path
from graphs.path import Path
import networkx as nx
import numpy as np
import logging

logging.getLogger().setLevel(logging.INFO)


# Algoritmo di Yen, genera i K cammini in ordine crescente, a partire dal cammino minimo.
# Il cammino minimo di partenza viene individuato con l'algoritmo di Dijakstra.
class YenKPaths:
    def __init__(self, sht_path: Path, wave_net: nx.DiGraph):
        self.wave_net = wave_net
        self.paths_container = [sht_path]

    def search_kpaths(self, k=10):
        candidati_container = []
        for k in range(1, k):

            candidati_container = self.core_algorithm(k, candidati_container)

            if not candidati_container:
                break
            sorted(candidati_container)
            new_entry = heappop(candidati_container)
            self.paths_container.append(new_entry)
            logging.info(f"Trovato cammino con costo: {new_entry.cost}")
        return self.paths_container

    def core_algorithm(self, k, candidati_container):
        cammino_precedente = self.paths_container[k - 1]
        for i in range(0, len(cammino_precedente.nodes) - 1):

            nodo_deviazione = cammino_precedente.nodes[i]
            # La parte inizale del cammino rimane invariata, a partire dal nodo di deviazione,
            # si cerca un altro cammino minimo
            root_path: [str] = cammino_precedente.nodes[:i + 1]

            copy_wave_net: nx.DiGraph = copy.deepcopy(self.wave_net)
            for nd in copy_wave_net.nodes:
                copy_wave_net.node[nd]['cost'] = np.inf
                copy_wave_net.node[nd]['prev'] = None

            # Cerco nel container dei cammini trovati fin ora, i cammini che hanno la parte iniziale
            # uguale alla parte iniziale del cammino in esame, ed elimino gli archi uscenti
            # per tutti cammini dal nodo deviazione.
            for path in self.paths_container:
                control_path: Path = path

                if root_path == control_path.nodes[:i + 1]:
                    try:
                        copy_wave_net.remove_edge(control_path.nodes[i], control_path.nodes[i + 1])
                    except Exception as exn:
                        logging.debug('Arco già eliminiato, si prosegue!')
                        continue

            # Elimino dalla rete i nodi appartenti alla parte iniziale del cammino,
            # tranne il nodo di deviazione
            for root_path_node in root_path:
                if root_path_node != nodo_deviazione:
                    copy_wave_net.remove_node(root_path_node)

            # WavelengthNet.plot_graph(copy_wave_net.edges, copy_wave_net.nodes, copy_wave_net.graph)
            # Ricerco il cammino minimo dal nodo deviazione fino al target.
            new_dj = DijakstraPQ(copy_wave_net)
            new_dj.source = nodo_deviazione
            cammino_deviato = None
            try:
                cammino_deviato = new_dj.search_paths()
            except Exception as ex:
                logging.debug(str(ex))
                continue

            if len(cammino_deviato.nodes) > 1:
                cost_root_path = sum([self.wave_net.get_edge_data(u,v)[0]['weight'] for u,v in zip(root_path, root_path[1:])])
                heappush(candidati_container, Path(root_path[:-1] + cammino_deviato.nodes, cammino_deviato.cost + cost_root_path))

        return candidati_container


@click.command()
@click.option('--k', default=10, help='Numero massimo di cammini minimi da cercare.')
@click.option('--limit_radii', default=16, help='Numero di raggi da inserire nel grafo.')
@click.option('--nodes_in_radii', default=120, help='Numero di nodi per ogni raggio.')
@click.option('--tau', default=20, help='Peso massimo che un arco può raggiungere.')
def search_k_shortest_paths(k, limit_radii, nodes_in_radii, tau):
    from graphs.wavenet import WaveNet
    from graphs.risonanze import DATA, SOURCE, TARGET

    wavelength_net = WaveNet(DATA, SOURCE, TARGET)
    wavelength_net.select_rays(limit_radii=limit_radii, num_nodes_per_radii=nodes_in_radii)
    wavelength_net.create_graph(tau=tau, )
    wavelength_net.add_cost_attributes()

    bc_pq = DijakstraPQ(wavelength_net.G)
    try:
        sht_path = bc_pq.search_paths()
    except Exception as ex:
        click.secho(str(ex), fg='red', bold=True)
        return

    yen_kpaths = YenKPaths(sht_path, wavelength_net.G)
    paths = yen_kpaths.search_kpaths(k)

    for num, path in enumerate(paths):
        click.secho(f"Route {num+1}")
        print_path(path)


if __name__ == '__main__':
    search_k_shortest_paths()
