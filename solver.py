from abc import ABC, abstractmethod
from graphs.wavenet_model import WaveNetModel


class Solver(ABC):
    def __init__(self, wavenet: WaveNetModel) -> None:
        super().__init__()
        self.net = wavenet
        self._obj_value = None

    @abstractmethod
    def get_runtime(self) -> float:
        pass

    def set_obj_value(self, value: float) -> None:
        print(f"Objective value: {value}")
        self._obj_value = value

    def obj_value(self) -> float:
        return self._obj_value

    @abstractmethod
    def solve_model(self) -> None:
        pass

    @abstractmethod
    def get_path(self):
        pass

    @staticmethod
    @abstractmethod
    def log_filename():
        pass
