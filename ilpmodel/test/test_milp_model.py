from abc import ABC, abstractmethod


class TestMilpModel(ABC):
    @abstractmethod
    def setUpClass(cls): pass

    def test_existence_edge_var_attributes(self):
        for edge in self.model.net.G.edges.data():
            self.assertIn('var', edge[2])

    def test_existence_node_var_attributes(self):
        for node in self.model.net.G.nodes.data():
            self.assertIn('var', node[1])

    def test_edge_var_are_zero_or_one(self):
        for edge in self.model.net.G.edges.data():
            self.assertIn(self.model.get_var_value(edge[2]['var']), [0,1])

    def test_node_var_are_zero_or_one(self):
        for node in self.model.net.G.nodes.data():
            self.assertIn(self.model.get_var_value(node[1]['var']), [0,1])

    def test_edge_var_sum_out_node_eq_zero_or_one(self):
        for nd in self.model.net.G.nodes.data():
            var_list = map(lambda edg: self.model.get_var_value(self.model.net.G.get_edge_data(edg[0], edg[1])['var']),
                           self.model.net.G.out_edges(nd[0]))
            self.assertIn(sum(var_list), [0, 1.0])

    def test_source_node_out_var_edges_equal_one(self):
        out_edges = map(lambda edg: self.model.get_var_value(self.model.net.G.get_edge_data(edg[0], edg[1])['var']),
                           self.model.net.G.out_edges(self.model.net.source))

        self.assertEqual(sum(out_edges), 1.0)

    def test_target_node_in_var_edges_equal_one(self):
        in_edges = map(lambda edg: self.model.get_var_value(self.model.net.G.get_edge_data(edg[0], edg[1])['var']),
                           self.model.net.G.in_edges(self.model.net.target))

        self.assertEqual(sum(in_edges), 1.0)

    def test_source_node_var_equal_one(self):
        self.assertEqual(self.model.net.G.node[self.model.net.source]['var'], 1.0)

    def test_target_node_var_equal_one(self):
        self.assertEqual(self.model.get_var_value(self.model.net.G.node[self.model.net.target]['var']), 1.0)

    def test_edge_var_sum_out_node_eq_var_in_node(self):
        for nd in self.model.net.G.nodes.data():
            if nd[0] != self.model.net.target:
                var_list = map(lambda edg: self.model.get_var_value(self.model.net.G.get_edge_data(edg[0], edg[1])['var']),
                               self.model.net.G.out_edges(nd[0]))
                self.assertEqual(sum(var_list), self.model.get_var_value(self.model.net.G.node[nd[0]]['var']))

    def test_edge_var_sum_in_node_eq_var_in_node(self):
        for nd in self.model.net.G.nodes.data():
            if nd[0] != self.model.net.source:
                var_list = map(lambda edg: self.model.get_var_value(self.model.net.G.get_edge_data(edg[0], edg[1])['var']),
                               self.model.net.G.in_edges(nd[0]))
                self.assertEqual(sum(var_list), self.model.get_var_value(self.model.net.G.node[nd[0]]['var']))

    def test_radii_var_eq_zero_or_one(self):
        for rd in self.model.radii_vars:
            self.assertIn(self.model.get_var_value(rd[0]), [0, 1.0])

    def test_radii_var_sum_eq_num_radii(self):
        self.assertEqual(sum(map(lambda x: self.model.get_var_value(x[0]), self.model.radii_vars)), self.nrays)

    def test_node_var_less_or_eq_var_ray(self):
        for nd in self.model.net.G.nodes.data():
            if nd[0] != self.model.net.source and nd[0] != self.model.net.target:
                rd_var = next(obj for obj in self.model.radii_vars if obj[1] == nd[1]['ray'])
                self.assertLessEqual(self.model.get_var_value(nd[1]['var']), self.model.get_var_value(rd_var[0]))

    def test_if_node_selected_also_ray_selected(self):
        for nd in self.model.net.G.nodes.data():
            if nd[0] != self.model.net.source and nd[0] != self.model.net.target:
                if self.model.get_var_value(nd[1]['var']) == 1:
                    rd_var = next(obj for obj in self.model.radii_vars if obj[1] == nd[1]['ray'])
                    self.assertEqual(self.model.get_var_value(nd[1]['var']), self.model.get_var_value(rd_var[0]))

    def test_if_ray_selected_exact_resonances_are_selected(self):
        for rvar in self.model.radii_vars:
            filter_nodes = list(filter(lambda nd: nd[1]['ray'] == rvar[1], self.model.net.G.nodes.data()))

            self.assertEqual(sum([self.model.get_var_value(nd_var[1]['var']) for nd_var in filter_nodes]),
                             (self.model.net.resonances * rvar[0]))

    def test_num_selected_nodes_are_nray_x_peacks(self):
        nodes_in_path = list(filter(lambda nd: self.model.get_var_value(nd[1]['var']) == 1, self.model.net.G.nodes.data()))
        self.assertEqual(len(nodes_in_path), (self.model.net.radii*self.model.net.resonances)+2)

    def test_source_in_selected_path(self):
        nodes_in_path = list(filter(lambda nd: self.model.get_var_value(nd[1]['var']) == 1, self.model.net.G.nodes.data()))
        path_nodes = list(map(lambda x: x[0], nodes_in_path))
        self.assertIn(self.model.net.source, path_nodes)

    def test_target_in_selected_path(self):
        nodes_in_path = list(filter(lambda nd: self.model.get_var_value(nd[1]['var']) == 1, self.model.net.G.nodes.data()))
        path_nodes = list(map(lambda x: x[0], nodes_in_path))
        self.assertIn(self.model.net.target, path_nodes)

    def test_num_selected_edges_are_nray_x_peacks_and_one(self):
        edges_in_path = list(filter(lambda nd: self.model.get_var_value(nd[2]['var']) == 1, self.model.net.G.edges.data()))
        self.assertEqual(len(edges_in_path), (self.model.net.radii*self.model.net.resonances)+1)

    def test_path_is_continuos_route(self):
        path = self.model.get_path()
        self.assertTrue(path.has_source_to_target_path())