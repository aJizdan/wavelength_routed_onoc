import unittest
from ilpmodel.gurobi_milp import GrbIlp
from incremental_resolution import IncrementalSolver
from graphs.netfactory_onoc import ONOCNetFactory


class TestGurobiMilpModelTau(unittest.TestCase):

    def test_incremental_tau_on_gurobi_4x1(self):
        inc_model = IncrementalSolver(GrbIlp, ONOCNetFactory, 4, 1)
        inc_model.find_optimal()

        for i in range(len(inc_model.incremental_models)-1):
            if inc_model.incremental_models[i].obj_value() != -1:
                self.assertGreaterEqual(inc_model.incremental_models[i].obj_value(),
                                        inc_model.incremental_models[i+1].obj_value())

    def test_incremental_tau_on_gurobi_4x4(self):
        inc_model = IncrementalSolver(GrbIlp, ONOCNetFactory, 4, 4)
        inc_model.find_optimal()

        for i in range(len(inc_model.incremental_models)-1):
            if inc_model.incremental_models[i].obj_value() != -1:
                self.assertGreaterEqual(inc_model.incremental_models[i].obj_value(),
                                        inc_model.incremental_models[i+1].obj_value())

    def test_incremental_tau_on_gurobi_8x1(self):
        inc_model = IncrementalSolver(GrbIlp, ONOCNetFactory, 8, 1)
        inc_model.find_optimal()

        for i in range(len(inc_model.incremental_models)-1):
            if inc_model.incremental_models[i].obj_value() != -1:
                self.assertGreaterEqual(inc_model.incremental_models[i].obj_value(),
                                        inc_model.incremental_models[i+1].obj_value())

    def test_incremental_tau_on_gurobi_8x4(self):
        inc_model = IncrementalSolver(GrbIlp, ONOCNetFactory, 8, 4)
        inc_model.find_optimal()

        for i in range(len(inc_model.incremental_models)-1):
            if inc_model.incremental_models[i].obj_value() != -1:
                self.assertGreaterEqual(inc_model.incremental_models[i].obj_value(),
                                        inc_model.incremental_models[i+1].obj_value())
