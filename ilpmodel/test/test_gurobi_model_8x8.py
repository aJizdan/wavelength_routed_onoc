import unittest
from ilpmodel.gurobi_milp import GrbIlp


class TestGurobiMilpModel8x8(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory
        cls.nrays = 8
        cls.peaks = 8
        c = cf.NetConfig()
        cls.netfactory = ONOCNetFactory(c)
        network = cls.netfactory.make_wave_net(cls.nrays, cls.peaks)

        cls.model = GrbIlp(network)

    def test_model_INFEASIBLE(self):
        with self.assertRaises(ValueError):
            self.model.solve_model()
