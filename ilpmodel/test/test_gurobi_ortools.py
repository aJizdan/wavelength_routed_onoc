import unittest
from ilpmodel.gurobi_milp import GrbIlp
from ilpmodel.ortools_milp import WSPIlp


class TestGurobiMilpModelTau(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory
        cls.nrays = 4
        cls.peaks = 4
        c = cf.NetConfig()
        cls.netfactory = ONOCNetFactory(c)

        cls.f = open("gurobi_result.log", "w+")
        cls.f.write(f"{'Dimensioni':^10} {'Delta':^10} "
                     f"{'Tau':^10} {'NumArchi':^10} {'Time':^10} {'CostoSoluzione':^20}\n")

    def test_model_different_solver_same_path_cost(self):
        network = self.netfactory.make_wave_net(self.nrays, self.peaks)
        model_gr = GrbIlp(network)
        model_gr.solve_model()

        model = WSPIlp(network)
        model.solve_model()

        self.assertEqual(str(model.obj_value())[:4], str(model_gr.obj_value())[:4])

    def test_model_different_solver_same_path_cost_tau_3(self):
        tau = 3
        network = self.netfactory.make_wave_net(self.nrays, self.peaks, tau=tau)
        model_gr = GrbIlp(network)
        model_gr.solve_model()

        model = WSPIlp(network)
        model.solve_model()

        self.f.write(f"{str(network.radii) + 'x' + str(network.resonances):^10} {str(network.delta)[:4]:>10} "
                     f"{network.tau:>10} "
                     f"{len(network.G.edges):>10} "
                     f"{str(model.get_runtime())[:5]:>10} "
                     f"{str(model.obj_value())[:4]:>10}\n")

        self.assertEqual(str(model.obj_value())[:4], str(model_gr.obj_value())[:4])
