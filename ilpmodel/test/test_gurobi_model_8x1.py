import unittest
from ilpmodel.test.test_milp_model import TestMilpModel
from ilpmodel.gurobi_milp import GrbIlp


class TestGurobiMilpModel8x1(unittest.TestCase, TestMilpModel):
    @classmethod
    def setUpClass(cls):
        import graphs.risonanze as cf
        from graphs.netfactory_onoc import ONOCNetFactory
        cls.nrays = 8
        cls.peaks = 1
        c = cf.NetConfig()
        cls.netfactory = ONOCNetFactory(c)
        network = cls.netfactory.make_wave_net(cls.nrays, cls.peaks)

        cls.model = GrbIlp(network)
        cls.model.solve_model()