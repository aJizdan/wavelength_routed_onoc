from abc import abstractmethod
from solver import Solver
from graphs.wavenet_model import WaveNetModel


class SolverModel(Solver):
    def __init__(self, wavenet: WaveNetModel) -> None:
        Solver.__init__(self, wavenet)
        self.radii_vars = None

    def _add_solver_constraints(self):
        # somma delle Var sugli archi uscenti da ogni nodo deve essere ugale a quelli entranti
        for node in self.net.G.nodes:
            if node != self.net.target and node != self.net.source:
                self.add_constraint()(self.summation()([ed[2]['var'] for ed in self.net.G.edges(node, data=True)]) ==
                                      self.summation()([ed[2]['var'] for ed in self.net.G.in_edges(node, data=True)]),
                                      f"flow_balance_{node}")

        # somma delle Var sugli archi uscenti da source deve essere == 1
        self.add_constraint()(
            self.summation()([ed[2]['var'] for ed in self.net.G.edges(self.net.source, data=True)]) == 1,
            "Source")
        self.add_constraint()(self.net.G.node[self.net.source]['var'] == 1, "SourceNode")

        # somma delle Var sugli archi entranti in target deve essere == 1
        self.add_constraint()(
            self.summation()([ed[2]['var'] for ed in self.net.G.in_edges(self.net.target, data=True)]) == 1,
            "Target")
        self.add_constraint()(self.net.G.node[self.net.target]['var'] == 1, "TargetNode")

        # somma degli archi uscenti da un nodo è una varibile booleana
        for node in self.net.G.nodes:
            if node != self.net.target:
                self.add_constraint()(self.summation()([ed[2]['var'] for ed in self.net.G.edges(node, data=True)]) ==
                                      self.net.G.node[node]['var'])

        # Assicuro che esattamente nradii raggi vengano selezionati
        self.add_constraint()(self.summation()([rvar[0] for rvar in self.radii_vars]) == self.net.radii)

        for rvar in self.radii_vars:
            filter_nodes = list(filter(lambda nd: nd[1]['ray'] == rvar[1], self.net.G.nodes.data()))

            self.add_constraint()(
                self.summation()([nd_var[1]['var'] for nd_var in filter_nodes]) == (self.net.resonances * rvar[0]))

            for rnode in filter_nodes:
                self.add_constraint()(rnode[1]['var'] <= rvar[0])
                # Routing Faults Prevention
                for fault_radius in rnode[1]['conflict_radii']:
                    try:
                        fault_var = next(x for x in self.radii_vars if x[1] == fault_radius)
                        self.add_constraint()(rnode[1]['var'] + fault_var[0] <= 1)
                    except StopIteration:
                        raise ValueError(f"Trovato un raggio con ID non valido!")

    def get_path(self):
        import networkx as nx
        from graphs.wavenet_path import WaveNetPath
        edges_in_path = list(filter(lambda nd: self.get_var_value(nd[2]['var']) == 1, self.net.G.edges.data()))
        nodes_in_path = list(filter(lambda nd: self.get_var_value(nd[1]['var']) == 1, self.net.G.nodes.data()))
        P = nx.DiGraph()
        P.add_nodes_from(nodes_in_path)
        P.add_edges_from(edges_in_path)
        assert nx.has_path(P, self.net.source, self.net.target), ValueError("Il grafo del cammino non è completo!")
        path = WaveNetPath(self.net.radii, self.net.resonances, self.net.source, self.net.target, P)
        return path

    @abstractmethod
    def get_var_value(self, variable):
        pass

    @abstractmethod
    def _create_solver_vars(self):
        pass

    @abstractmethod
    def add_constraint(self):
        pass

    @abstractmethod
    def summation(self):
        pass
