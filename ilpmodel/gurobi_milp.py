from graphs.wavenet_model import WaveNetModel
from ilpmodel.solver_model import SolverModel
from gurobipy import *


class GrbIlp(SolverModel):
    def __init__(self, onoc_net: WaveNetModel):
        SolverModel.__init__(self, onoc_net)

        # Create a new model
        self.solver = Model("WSP")
        self._create_solver_vars()
        self._add_solver_constraints()

    def _create_solver_vars(self):

        for edge in self.net.G.edges.data():
            # Creo le variabili boolean per la selezione di un arco del grafo
            edge[2]['var'] = self.solver.addVar(vtype=GRB.BINARY, name=f"{edge[0]}__{edge[1]}")

        for node in self.net.G.nodes.data():
            node[1]['var'] = self.solver.addVar(vtype=GRB.BINARY, name=node[0])
        # Creo le variabli sui raggi (var, raggio)
        self.radii_vars = list(map(lambda rd: (self.solver.addVar(vtype=GRB.BINARY, name=rd), rd),
                                   self.net.get_net_radii()))

    def solve_model(self):
        # Objective minimizzare il costo del cammino
        self.solver.setObjective(sum([edge[2]['var'] * edge[2]['weight'] for edge in self.net.G.edges.data()]),
                                 GRB.MINIMIZE)

        # An integrality restriction on a variable is considered satisfied when the variable's value is less than
        # IntFeasTol from the nearest integer value. Tightening this tolerance can produce smaller integrality
        # violations, but very tight tolerances may significantly increase runtime.
        # Loosening this tolerance rarely reduces runtime. Only affects mixed integer programming(MIP) models
        # self.solver.Params.intfeastol = 1e-9
        # self.solver.setParam('TimeLimit', 30)
        # self.solver.read("gurobi.prm")
        try:
            self.solver.optimize()
        except GurobiError:
            self.set_obj_value(-2)
            raise ValueError("OUT OF MEMORY")

        try:
            assert self.solver.status == GRB.Status.OPTIMAL
            print('Optimal objective: %g' % self.solver.objVal)
            # assert self.solver.status != GRB.Status.INF_OR_UNBD
            # print('Model is infeasible or unbounded')
            # assert self.solver.status != GRB.Status.INFEASIBLE
            # print(f'Model is infeasible {self.solver.status}')
            # assert self.solver.status == GRB.Status.UNBOUNDED
            # print('Model is unbounded')
            assert self.get_path().total_cost() == self.solver.objVal, ValueError("Il costo trovato non "
                                                                                  "corrisponde al cost del cammino.")

            print('Optimization ended with status %d' % self.solver.status)
            print('Runtime %d' % self.get_runtime())
        except AssertionError:
            self.set_obj_value(-1)
            raise ValueError("MODEL INFEASIBLE")

        self.set_obj_value(self.solver.objVal)

    def get_var_value(self, variable):
        return variable.x

    def add_constraint(self):
        return self.solver.addConstr

    def summation(self):
        return sum

    def get_runtime(self):
        return self.solver.Runtime
    '''
    def obj_value(self) -> float:
        try:
            return self.solver.objVal
        except AttributeError:
            return -1

    '''
    @staticmethod
    def log_filename():
        return "gurobi_results"


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory

    c = cf.NetConfig(2)
    netfactory = ONOCNetFactory(c)

    net = netfactory.make_wave_net(2, 2)

    model = GrbIlp(net)
    model.solve_model()
    print(f"Path {model.get_path()}")
    print(f"Cost {model.get_path().total_cost()}")

    pass

