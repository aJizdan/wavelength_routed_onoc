from __future__ import print_function
from ortools.linear_solver import pywraplp
from ilpmodel.solver_model import SolverModel
from graphs.wavenet_model import WaveNetModel


class WSPIlp(SolverModel):
    def __init__(self, onoc_net: WaveNetModel):
        SolverModel.__init__(self, onoc_net)

        # Create a new model
        self.solver = pywraplp.Solver('SolveIntegerProblem', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
        # self.solver = pywraplp.Solver('BOP', pywraplp.Solver.BOP_INTEGER_PROGRAMMING)

        self._create_solver_vars()
        self._add_solver_constraints()

    def _create_solver_vars(self):
        for edge in self.net.G.edges.data():
            # Creo le variabili boolean per la selezione di un arco del grafo
            edge[2]['var'] = self.solver.BoolVar(f"{edge[0]}__{edge[1]}")

        for node in self.net.G.nodes.data():
            node[1]['var'] = self.solver.BoolVar(f"{node[0]}")
        # Creo le variabli sui raggi (var, raggio)
        self.radii_vars = list(map(lambda rd: (self.solver.BoolVar(f"{rd}"), rd),
                                   self.net.get_net_radii()))

    def solve_model(self):
        # self.solver.EnableOutput()
        # solver.SetSolverSpecificParametersAsString('prune_search_tree:true')
        # solver.SetSolverSpecificParametersAsString('use_random_lns:false')
        # solver.SetSolverSpecificParametersAsString('num_relaxed_vars:40')
        # solver.SetSolverSpecificParametersAsString('use_potential_one_flip_repairs_in_ls:true')
        # solver.SetSolverSpecificParametersAsString('use_lp_strong_branching:true')
        # solver.SetSolverSpecificParametersAsString('lp_max_deterministic_time:10.0')
        # Sets a time limit of 10 seconds.
        # self.solver.set_time_limit(1000)
        # Objective minimizzare il costo del cammino
        self.solver.Minimize(
            self.summation()(([edge[2]['var'] * edge[2]['weight'] for edge in self.net.G.edges.data()])))

        """Solve the problem and print the solution."""
        result_status = self.solver.Solve()

        try:
            # The problem has an optimal solution.
            assert result_status == pywraplp.Solver.OPTIMAL

            # The solution looks legit (when using solvers other than
            # GLOP_LINEAR_PROGRAMMING, verifying the solution is highly recommended!).
            assert self.solver.VerifySolution(1e-7, True)
            print('Number of variables =', self.solver.NumVariables())
            print('Number of constraints =', self.solver.NumConstraints())
            # The objective value of the solution.
            print(f"Optimal objective value = {self.solver.Objective().Value()}")
            print(f"{self.get_runtime()}")
        except AssertionError:
            raise ValueError("MODEL INFEASIBLE")

    def get_var_value(self, variable):
        return variable.solution_value()

    def add_constraint(self):
        return self.solver.Add

    def summation(self):
        return self.solver.Sum

    def get_runtime(self):
        return self.solver.WallTime()/1000.0

    def obj_value(self) -> float:
        if self.solver.Solve() != pywraplp.Solver.OPTIMAL:
            return -1
        return self.solver.Objective().Value()

    @staticmethod
    def log_filename():
        return "ortools_results"


if __name__ == '__main__':
    import graphs.risonanze as cf
    from graphs.netfactory_onoc import ONOCNetFactory

    c = cf.NetConfig(20)
    netfactory = ONOCNetFactory(c)

    net = netfactory.make_wave_net(4, 8)

    model = WSPIlp(net)
    model.solve_model()

    pt = model.get_path()

    pass
