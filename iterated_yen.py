import click
import logging
from graphs.wavenet import WaveNet
from heapq import *
from yen_algorithm import YenKPaths
from graphs.path import Path
from dijakstra_pq import DijakstraPQ, print_path


logging.getLogger().setLevel(logging.INFO)


class IteratedYen(YenKPaths):
    def __init__(self, sht_path: Path, wave_net: WaveNet):
        YenKPaths.__init__(self, sht_path, wave_net.G)
        self.wave_class = wave_net

    def search_resource_constrained_paths(self, ):
        candidati_container = []
        k = 1
        while not self.wave_class.check_required_path(self.paths_container[k-1]):

            candidati_container = self.core_algorithm(k, candidati_container)

            if not candidati_container:
                break
            sorted(candidati_container)
            k += 1
            new_entry = heappop(candidati_container)
            self.paths_container.append(new_entry)
            logging.info(f"Trovato cammino con costo: {new_entry.cost}")
        return self.paths_container


@click.command()
@click.option('--limit_radii', default=150, help='Numero di raggi da inserire nel grafo.')
@click.option('--nodes_in_radii', default=120, help='Numero di nodi per ogni raggio.')
@click.option('--tau', default=20, help='Peso massimo che un arco può raggiungere.')
def search_k_shortest_paths(limit_radii, nodes_in_radii, tau):
    from graphs.risonanze import DATA, SOURCE, TARGET

    wavelength_net = WaveNet(DATA, SOURCE, TARGET, nradii=4, nresonance=4)
    wavelength_net.select_rays(limit_radii=limit_radii, num_nodes_per_radii=nodes_in_radii)
    wavelength_net.create_graph(tau=tau, )
    wavelength_net.add_cost_attributes()
    wavelength_net.add_routing_fault_radii()

    bc_pq = DijakstraPQ(wavelength_net.G)
    try:
        sht_path = bc_pq.search_paths()
    except Exception as ex:
        click.secho(str(ex), fg='red', bold=True)
        return

    itr_yen = IteratedYen(sht_path, wavelength_net)
    paths = itr_yen.search_resource_constrained_paths()

    click.secho(f"Route {len(paths)}")
    print_path(paths[-1])


if __name__ == '__main__':
    search_k_shortest_paths()
