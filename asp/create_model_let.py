#!/usr/bin/python

import graphs.risonanze as cf
from graphs.netfactory_onoc import ONOCNetFactory

moltiplicatore = 10

def create_net_let():
    c = cf.NetConfig(26)
    netfactory = ONOCNetFactory(c)
    return netfactory.make_wave_net(4,4)


if __name__ == '__main__':
    net = create_net_let()

    f = open("graph.pl", "w+")

    f.write(f"radii({net.radii}).\n")
    f.write(f"resonances({net.resonances}).\n")
    f.write(f"path_length({net.radii*net.resonances+1}).\n")
    f.write(f"nodes_length({net.radii*net.resonances+2}).\n")
    f.write(f"source(x{net.source}).\n")
    f.write(f"target(x{net.target}).\n")

    rays = net.get_net_radii()
    for r in net.get_net_radii():
        f.write(f"radius({r}).\n")

    for nd in net.G.nodes.data():
        ray = 99
        if nd[1]['ray']:
            ray = nd[1]['ray']
        f.write(f"node(x{nd[0]}, {ray}).\n")
        for fault in nd[1]['conflict_radii']:
            f.write(f"conflict(x{nd[0]}, {fault}).\n")

    for ed in net.G.edges.data():
        f.write(f"edge(x{ed[0]}, x{ed[1]}, {int(ed[2]['weight']*moltiplicatore)}).\n")

    f.close()

