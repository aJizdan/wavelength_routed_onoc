import json
from asp.create_model_let import create_net_let, moltiplicatore
from incremental_resolution import IncrementalSolver
from dynamicprogramming.solver_base import SolverBase


log_filename = 'asp_result10'


def gen_dict_extract(key, var):
    if hasattr(var,'items'):
        for k, v in var.items():
            if k == key:
                yield v
            if isinstance(v, dict):
                for result in gen_dict_extract(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    for result in gen_dict_extract(key, d):
                        yield result



def log_to_file(solved_models, filename='results'):
    f = open(f"{filename}.txt", "a")
    f.write(f"{'Dimensioni':^10} {'Delta':^10} "
            f"{'Tau':^10} {'NumArchi':^10} {'Time':^10} {'CostoSoluzione':^20}\n")
    for result in solved_models:
        f.write(f"{result[0]:^10} "
                f"{result[1]:>10} "
                f"{result[2]:>10} "
                f"{result[3]:>10} "
                f"{result[4]:>10} "
                f"{result[5]:>10}\n")
    f.write(f"{'Dimensioni':^10} "
            f"{'Tau':^10} {'Time':^10} {'CostoSoluzione':^20} Path\n")
    for result in solved_models:
        f.write(f"{result[0]:^10} "
                f"{result[2]:>10} "
                f"{result[4]:>10} "
                f"{result[5]:>10} {result[6]}\n")

    f.write("\n")
    f.close()


if __name__ == '__main__':
    with open('asp_result.json', 'r') as f:
        net = create_net_let()
        baseSolver = SolverBase(net)

        datastore = json.load(f)
        asp_time = datastore['Time']['Total']
        solved_models = []
        if datastore['Result'] != "UNSATISFIABLE":
            key = 'Witnesses'
            result = list(gen_dict_extract(key, datastore))[0]


            optimal = result[-1]
            asp_cost = optimal['Costs'][0]
            str_node = 'selected_node'
            selected_nodes = [tuple(item.replace(str_node, '').replace('x', '').replace('(', '').replace(')', '').split(','))
                              for item in optimal['Value'] if item.startswith(str_node)]

            str_radius = 'selected_radius'
            selected_radius = [int(item.replace(str_radius, '').replace('(', '').replace(')', ''))
                              for item in optimal['Value'] if item.startswith(str_radius)]

            path_edges = [item.replace('x', '').replace('(', '').replace(')', '').split(',')
                              for item in optimal['Value'] if item.startswith('(x')]


            path_nodes = [net.source]
            init = net.source
            while len(path_nodes) != len(path_edges)+1:
                for edge in path_edges:
                    if edge[0] == init:
                        path_nodes.append(edge[1])
                        init = edge[1]

            wave_path = baseSolver._nodes_to_path(path_nodes)

            # IncrementalSolver.init_log_file(log_filename)

            # (f"{'Dimensioni':^10} {'Delta':^10} "f"{'Tau':^10} {'NumArchi':^10} {'Time':^10} {'CostoSoluzione':^20}\n")

            solved_models = [(f"{net.radii}x{net.resonances}", str(net.delta)[:4], str(net.tau)[:5], f"{len(net.G.edges())}",
                              f"{asp_time}", f"{str(wave_path.total_cost())[:4]} # {asp_cost}   (w*{moltiplicatore})",
                              f"{str(wave_path)}")]
        else:
            solved_models = [
                (f"{net.radii}x{net.resonances}", str(net.delta)[:4], str(net.tau)[:5], f"{len(net.G.edges())}",
                 f"{asp_time}", f"-1 (w*{moltiplicatore})", f"[]")]

        log_to_file(solved_models,log_filename)

        pass
