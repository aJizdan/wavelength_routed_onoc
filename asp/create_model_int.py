#!/usr/bin/python

import graphs.risonanze as cf
from graphs.netfactory_onoc import ONOCNetFactory

def create_net_int():
    c = cf.NetConfig(26)
    netfactory = ONOCNetFactory(c)
    return netfactory.make_wave_net(4, 4)

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


if __name__ == '__main__':
    net = create_net_int()

    f = open("graph.pl", "w+")

    f.write(f"radii({net.radii}).\n")
    f.write(f"resonances({net.resonances}).\n")
    f.write(f"path_length({net.radii*net.resonances+1}).\n")
    f.write(f"nodes_length({net.radii*net.resonances+2}).\n")
    f.write(f"source(0).\n")
    f.write(f"target(1).\n")
    f.write(f"node(0, 99).\n")
    f.write(f"node(1, 99).\n")

    rays = net.get_net_radii()
    for r in net.get_net_radii():
        f.write(f"radius({r}).\n")

    for nd in net.G.nodes.data():
        if nd[1]['ray']:
            ray = nd[1]['ray']
            f.write(f"node({int(ray)+1}{nd[1]['radius']}, {ray}).\n")
            for fault in nd[1]['conflict_radii']:
                f.write(f"conflict({int(ray)+1}{nd[1]['radius']}, {fault}).\n")

    for ed in net.G.edges.data():
        est1 = ed[0].replace('_', '')
        est2 = ed[1].replace('_', '')
        if is_number(est1) and is_number(est2):
            est1 = str(int(ed[0].split('_')[0])+1)
            est2 = str(int(ed[1].split('_')[0])+1)
            f.write(f"edge({est1}{ed[0].split('_')[1]}, {est2}{ed[1].split('_')[1]}, {int(ed[2]['weight']*10)}).\n")
        elif not is_number(est1):
            est2 = str(int(ed[1].split('_')[0]) + 1)
            f.write(f"edge(0, {est2}{ed[1].split('_')[1]}, {int(ed[2]['weight'] * 10)}).\n")
        elif not is_number(est2):
            est1 = str(int(ed[0].split('_')[0]) + 1)
            f.write(f"edge({est1}{ed[0].split('_')[1]}, 1, {int(ed[2]['weight'] * 10)}).\n")
        else:
            raise ValueError("Arco non valido")
    f.close()

