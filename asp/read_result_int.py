import json
from asp.create_model_int import create_net_int
from incremental_resolution import IncrementalSolver
from dynamicprogramming.solver_base import SolverBase


log_filename = 'asp_result_int'


def gen_dict_extract(key, var):
    if hasattr(var,'items'):
        for k, v in var.items():
            if k == key:
                yield v
            if isinstance(v, dict):
                for result in gen_dict_extract(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    for result in gen_dict_extract(key, d):
                        yield result


def log_to_file(solved_models, filename='results'):
    f = open(f"{filename}.txt", "a")
    f.write(f"{'Dimensioni':^10} {'Delta':^10} "
            f"{'Tau':^10} {'NumArchi':^10} {'Time':^10} {'CostoSoluzione':^20}\n")
    for result in solved_models:
        f.write(f"{result[0]:^10} "
                f"{result[1]:>10} "
                f"{result[2]:>10} "
                f"{result[3]:>10} "
                f"{result[4]:>10} "
                f"{result[5]:>10}\n")
    f.write(f"{'Dimensioni':^10} "
            f"{'Tau':^10} {'Time':^10} {'CostoSoluzione':^20} Path\n")
    for result in solved_models:
        f.write(f"{result[0]:^10} "
                f"{result[2]:>10} "
                f"{result[4]:>10} "
                f"{result[5]:>10} {result[6]}\n")

    f.write("\n")
    f.close()

if __name__ == '__main__':
    with open('asp_result.json', 'r') as f:
        net = create_net_int()
        baseSolver = SolverBase(net)

        datastore = json.load(f)
        asp_time = datastore['Time']['Total']
        solved_models = []
        if datastore['Result'] != "UNSATISFIABLE":
            key = 'Witnesses'
            result = list(gen_dict_extract(key, datastore))[0]


            optimal = result[-1]
            asp_cost = optimal['Costs'][0]
            str_node = 'selected_node'
            selected_nodes = [tuple(item.replace(str_node, '').replace('(', '').replace(')', '').split(','))
                              for item in optimal['Value'] if item.startswith(str_node)]
            selected_nodes = [item for item in selected_nodes if int(item[0]) > 1]
            selected_nodes = [(item[1] + '_' + item[0][-4:], item[1], item[0]) for item in selected_nodes]
            selected_nodes.append(('s_1491', '99', '0'))
            selected_nodes.append(('t_1611', '99', '1'))

            dict_nodes = dict([(item[2], item[0] ) for item in selected_nodes])

            str_radius = 'selected_radius'
            selected_radius = [int(item.replace(str_radius, '').replace('(', '').replace(')', ''))
                              for item in optimal['Value'] if item.startswith(str_radius)]

            path_edges = [item.replace('(', '').replace(')', '').split(',')
                              for item in optimal['Value'] if item.startswith('(')]

            path_edges = [[dict_nodes[edge[0]], dict_nodes[edge[1]]] for edge in path_edges]
            path_nodes = [net.source]
            init = net.source
            while len(path_nodes) != len(path_edges)+1:
                for edge in path_edges:
                    if edge[0] == init:
                        path_nodes.append(edge[1])
                        init = edge[1]

            wave_path = baseSolver._nodes_to_path(path_nodes)

            # IncrementalSolver.init_log_file(log_filename)

            # (f"{'Dimensioni':^10} {'Delta':^10} "f"{'Tau':^10} {'NumArchi':^10} {'Time':^10} {'CostoSoluzione':^20}\n")

            solved_models = [(f"{net.radii}x{net.resonances}", str(net.delta)[:4], str(net.tau)[:5], f"{len(net.G.edges())}",
                              f"{asp_time}", f"{str(wave_path.total_cost())[:4]} # {asp_cost}", f"{str(wave_path)}")]
        else:
            solved_models = [
                (f"{net.radii}x{net.resonances}", str(net.delta)[:4], str(net.tau)[:5], f"{len(net.G.edges())}",
                 f"{asp_time}", f"-1", f"[]")]

        log_to_file(solved_models,log_filename)

        pass
