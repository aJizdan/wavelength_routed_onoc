%
% Wavelegth Selection Problem
%

{ path(X,Y) } :- edge(X,Y,C).
{ selected_node(E, R) } :- node(E, R).
{ selected_radius(R) } :- radius(R).

% Da ogni nodo intermedio selezionato parte un solo arco
:- selected_node(X, R), { path(X, _) } != 1, R < 99.
% In ogni nodo intermedio selezionato arriva un solo arco
:- selected_node(Y, R), { path(_, Y) } != 1, R < 99.

% Se un nodo è selezionato deve essere selezionato anche il raggio
:- selected_node(_, R), not selected_radius(R), R < 99.
% Per ogni raggio selezionato devono essere selezionati N picchi
:- { selected_node(_, R) } != N, resonances(N), selected_radius(R).

% Se un arco viene selezionato devono essere selezionati anche i suoi estremi
:- not selected_node(E, _), path(L,E).
:- not selected_node(E, _), path(E,L).

% La lunghezza del cammino è fissata
:- { path(X,Y) } != K, path_length(K).
% Un solo arco deve scire dal nodo source
:- { path(X, _) } != 1, source(X).
% Un solo arco deve entrare nel nodo target
:- { path(_, Y) } != 1, target(Y).

% Se seleziono un nodo escludi il raggio con cui è in conflitto
:- selected_node(E, _), conflict(E, R), selected_radius(R).

% I nodi devono avere una determinata cardinalità
:- { selected_node(_, _) } != N, nodes_length(N).

% Imposto il numero di raggi da selezionare
:- { selected_radius(_) } != N, radii(N).

% Funzione obiettivo, minimizza il costo del cammino
#minimize { C,X,Y : path(X,Y), edge(X,Y,C) }.

#show selected_radius/1.
#show selected_node/2.
#show (X,Y) : path(X,Y).

% clingo waveselection.pl graph.pl --outf 2 > asp_result.json
