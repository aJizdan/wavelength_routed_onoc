from collections import defaultdict

class Label:
    def __init__(self, id, cost, nodo, prev_node, prev_oid, ray_trace):
        self.id = id
        self.nodo = nodo
        self.cost = cost
        self.prev_node = prev_node
        self.prev_oid = prev_oid
        if ray_trace is None:
            self.ray_trace = defaultdict()
        else:
            self.ray_trace = ray_trace

    def __eq__(self, other):
        return self.id == other.id

    def __lt__(self, other):
        return self.cost < other.cost
